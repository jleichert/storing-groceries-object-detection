#include <pcl/io/pcd_io.h>
//#include <ros/ros.h>
#include <ros/console.h>                                 // for LogLocation
#include <ros/duration.h>                                // for Duration
#include <ros/init.h>                                    // for init, spin
#include <ros/node_handle.h>                             // for NodeHandle
#include <ros/publisher.h>                               // for Publisher
#include <ros/service_server.h>                          // for ServiceServer
#include <ros/time.h>                                    // for operator<<
#include <ros/topic.h>                                   // for waitForMessage
#include <tf2_ros/transform_listener.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgcodecs.hpp>
#include <pcl_conversions/pcl_conversions.h> // for fromROSMsg
#include <pcl/common/transforms.h> // for transformPointClouds
#include <dynamic_reconfigure/server.h>
#include <Eigen/src/Geometry/Transform.h>                // for Isometry3d
#include <stddef.h>                                      // for NULL
#include <stdint.h>                                      // for uint32_t
#include <boost/smart_ptr/shared_ptr.hpp>                // for shared_ptr
#include <ostream>                                       // for operator<<
#include <string>                                        // for string, char...
#include <image_geometry/pinhole_camera_model.h>         // for PinholeCamer...
#include <pcl/exceptions.h>                              // for IOException
#include <pcl/point_cloud.h>                             // for PointCloud
#include <tf2_ros/buffer.h>                              // for Buffer
#include <tf2_eigen/tf2_eigen.h>
#include <actionlib/server/simple_action_server.h>
#include <moveit/move_group_interface/move_group_interface.h>
// msgs
#include <clf_grasping_msgs/FindObjectsInROI.h>
#include <clf_grasping_msgs/FindTable.h>
#include <vision_msgs/Detection3DArray.h>
#include <std_srvs/Empty.h>
//#include <moveit_msgs/CollisionObject.h>
#include <vision_msgs/BoundingBox3D.h>                   // for BoundingBox3D
#include <sensor_msgs/CameraInfo.h>                      // for CameraInfo
#include <sensor_msgs/Image.h>                           // for Image
#include <storing_groceries_msgs/PlaceOnRectangleAction.h>
#include <storing_groceries_msgs/GetPlaceRectangle.h>
#include <storing_groceries_msgs/FindShelfAndObjects.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
// own
#include "high_level_functions.hpp"
#include "object_knowledge.hpp"
#include "point_clouds.hpp"
#include "util.hpp" // for getIsometryTransform
#include "pick_place.hpp"
//#include "topic2.h"
#include "topic2.hpp"
#include <storing_groceries_node/dyn_reconfConfig.h>

#define BASE_FRAME "base_footprint"

ros::NodeHandle *nh;
tf2_ros::Buffer tfBuffer(ros::Duration(15.0)); // Default is 10
tf2_ros::TransformListener *tfListener;
ros::Publisher detection_pub;
actionlib::SimpleActionServer<storing_groceries_msgs::PlaceOnRectangleAction> *place_as;
moveit::planning_interface::MoveGroupInterface *group;

bool makeTableToBox = true;
float tableSafetyMargin = 0.07;
bool makeAllStandardCylinder = false;
bool useNewObjectMethod = true;
std::string learnObjectsPath = "";
std::string pointcloud_topic;
std::string rgb_image_topic;
std::string depth_image_topic="/xtion/depth_registered/image_raw"; // TODO make reconfigurable?
const std::string rgb_camera_info_topic = "/xtion/rgb/camera_info"; // TODO make reconfigurable?
image_geometry::PinholeCameraModel * cam_model;

// For dynamic reconfigure
void reconfigure_callback(grasping_object_recognition::dyn_reconfConfig &config,
                          uint32_t level) {
  makeTableToBox = config.makeTableToBox;
  tableSafetyMargin = config.tableSafetyMargin;
  makeAllStandardCylinder = config.makeAllStandardCylinder;
  learnObjectsPath = config.learnObjectsPath;
  //TODO test if learnObjectsPath ends with a "/"?
  pointcloud_topic = config.pointcloud_topic;
  rgb_image_topic = config.rgb_image_topic;
  useNewObjectMethod = config.useNewObjectMethod;
  set_high_level_options(config.useAlternativePrimitiveFitting);
  if((level&1)!=0)
    parseConfigYAML(config.objects_config_path);
  
  ROS_INFO("Got reconfigure call, learnObjectsPath is now >%s<, makeTableToBox is now %s, "
           "tableSafetyMargin is now %f, makeAllStandardCylinder is now %s, pointcloud_topic is now %s, rgb_image_topic is now %s, objects_config_path is now %s\n",
           learnObjectsPath.c_str(), (makeTableToBox ? "true" : "false"), tableSafetyMargin, (makeAllStandardCylinder ? "true" : "false"), pointcloud_topic.c_str(), rgb_image_topic.c_str(), config.objects_config_path.c_str());
}

pcl::PointCloud<point_t>::Ptr moveHeadAround() {
  const double leftright=0.5;
  const double down=-0.9;
  const double up=-0.1;
  actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> ac(
      "/head_controller/follow_joint_trajectory", true);
  ac.waitForServer();
  control_msgs::FollowJointTrajectoryGoal goal;
  goal.trajectory.joint_names.push_back("head_1_joint");
  goal.trajectory.joint_names.push_back("head_2_joint");
  goal.trajectory.points.resize(1);
  goal.trajectory.points[0].positions.resize(2);
  goal.trajectory.points[0].positions[0]=leftright;
  goal.trajectory.points[0].positions[1]=down;
  goal.trajectory.points[0].time_from_start = ros::Duration(3, 0);
  ac.sendGoal(goal);
  ac.waitForResult();
  pcl::PointCloud<point_t>::Ptr cloud1 = getPointCloud(*nh, tfBuffer, pointcloud_topic, true, nullptr);
  goal.trajectory.points[0].positions[0]=leftright;
  goal.trajectory.points[0].positions[1]=up;
  ac.sendGoal(goal);
  ac.waitForResult();
  pcl::PointCloud<point_t>::Ptr cloud2 = getPointCloud(*nh, tfBuffer, pointcloud_topic, true, nullptr);
  goal.trajectory.points[0].positions[0]=-leftright;
  goal.trajectory.points[0].positions[1]=up;
  ac.sendGoal(goal);
  ac.waitForResult();
  pcl::PointCloud<point_t>::Ptr cloud3 = getPointCloud(*nh, tfBuffer, pointcloud_topic, true, nullptr);
  goal.trajectory.points[0].positions[0]=-leftright;
  goal.trajectory.points[0].positions[1]=down;
  ac.sendGoal(goal);
  ac.waitForResult();
  pcl::PointCloud<point_t>::Ptr cloud4 = getPointCloud(*nh, tfBuffer, pointcloud_topic, true, nullptr);
  *cloud1 += *cloud2;//TODO maybe some ICP here?
  *cloud1 += *cloud3;
  *cloud1 += *cloud4;
  try {
    pcl::io::savePCDFile ("/tmp/stitched_cloud.pcd", *cloud1, true);
  } catch (pcl::IOException &e) {
    ROS_WARN("%s", e.what());
  }
  ROS_INFO("moveHeadAround() finished");
  return cloud1;
}

bool findTableService(clf_grasping_msgs::FindTable::Request &req,
                      clf_grasping_msgs::FindTable::Response &res) {
  ROS_INFO("findTableService() called");
  
  // Get camera info and point cloud messages
  if(cam_model == NULL) {// only do this once, since it does not change
      ROS_INFO("Getting camera info ... (5s timeout)");
      boost::shared_ptr<sensor_msgs::CameraInfo const> sharedCameraInfo = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(rgb_camera_info_topic, *nh, ros::Duration(5, 0));
      ROS_INFO("done");
      if (sharedCameraInfo != NULL) {
          cam_model=new image_geometry::PinholeCameraModel();
          cam_model->fromCameraInfo(*sharedCameraInfo);
      } else {
          ROS_WARN("Failed to get camera info");
      }
  }
  
  //This is a possible alternative for getPointCloud() below. But not sure if it is faster, probably depends, e.g. on network conditions
  //ROS_INFO("Waiting for depth image message");
  //boost::shared_ptr<sensor_msgs::Image const> sharedDepthImage = ros::topic::waitForMessage<sensor_msgs::Image>(depth_image_topic, *nh, ros::Duration(10, 0));
  //if(sharedDepthImage==NULL) {
  //    ROS_ERROR("Failed to get a depth image");
  //    return false;
  //}
  //std::string orig_frame=sharedDepthImage->header.frame_id;
  //pcl::PointCloud<point_t>::Ptr raw_point_cloud=depthImageToPointcloud(*sharedDepthImage, NULL, *cam_model);
  //ROS_INFO_STREAM("Got depth image with stamp " << sharedDepthImage->header.stamp);
  //Eigen::Isometry3d transform=getIsometryTransform(tfBuffer, sharedDepthImage->header.frame_id, BASE_FRAME);
  //ROS_INFO("Transforming point cloud to %s", BASE_FRAME);
  //pcl::transformPointCloud(*raw_point_cloud, *raw_point_cloud, isometry2affine(transform.inverse()));//TODO this seems to take some time, maybe there is a solution where this is not necessary? Or maybe it's faster when source and target are not the same?
  //ROS_INFO("Transformation done");
  // Make unorganized
  //raw_point_cloud->width = (raw_point_cloud->width) * (raw_point_cloud->height);
  //raw_point_cloud->height = 1;


  std::string orig_frame;
  pcl::PointCloud<point_t>::Ptr raw_point_cloud = getPointCloud(*nh, tfBuffer, pointcloud_topic, true/*false*/, &orig_frame);//TODO depth image instead of point cloud? Or uncolored points?
  if(raw_point_cloud==NULL || raw_point_cloud->points.size()==0) {
      ROS_ERROR("Failed to get a point cloud");
      return false;
  }
  ROS_INFO("Got point cloud, now continuing with getCloudROI");
  
  /*
  // This is a possible replacement for the other getCloudROI function. The difference is that point_cloud does not have to be in BASE_FRAME, thus possibly reducing the cost for transforming the whole cloud
  Eigen::Isometry3d transform=getIsometryTransform(tfBuffer, orig_frame, BASE_FRAME);
  vision_msgs::BoundingBox3D crop_roi;
  crop_roi.size.x = 3.0;
  crop_roi.size.y = 4.0;
  crop_roi.size.z = 1.9;
  Eigen::Vector3d new_pos=transform*(*new Eigen::Vector3d(1.5, 0.0, 1.05));
  crop_roi.center.position = tf2::toMsg(new_pos);
  crop_roi.center.orientation = tf2::toMsg(*new Eigen::Quaterniond(transform.rotation()));
  pcl::PointCloud<point_t>::Ptr point_cloud = getCloudROI(raw_point_cloud, crop_roi); // Remove ground
  ROS_INFO("Calling transformPointCloud ...");
  pcl::transformPointCloud(*point_cloud, *point_cloud, isometry2affine(transform.inverse()));
  ROS_INFO("done");
  */
  pcl::PointCloud<point_t>::Ptr point_cloud = getCloudROI(raw_point_cloud, 0.0, 3.0, -2.0, 2.0, 0.1, 2.0); // Remove ground
  try {
    pcl::io::savePCDFile ("/tmp/cloud_filtered_ROI.pcd", *point_cloud, true);
  } catch (pcl::IOException &e) {
    ROS_WARN("%s", e.what());
  }
  
  plane_t * table=searchAndAddTableToScene(point_cloud, orig_frame, cam_model, true, req.addToPlanningScene, makeTableToBox, tableSafetyMargin); // Here point_cloud must be in BASE_FRAME
  if(table != NULL) {
    res.plane = table->plane;
    res.bbox = table->bbox;
    res.hull = table->hull;
    delete table;
  } else {
    ROS_WARN("findTableService() finished, but no table found!");
    return false;
  }
  ROS_INFO("findTableService() finished");
  return true;
}

bool findShelfService(clf_grasping_msgs::FindTable::Request &req,
                      clf_grasping_msgs::FindTable::Response &res) {
  ROS_INFO("findShelfService() called");

  // Get camera info and point cloud messages
  if(cam_model == NULL) {// only do this once, since it does not change
      ROS_INFO("Getting camera info ... (5s timeout)");
      boost::shared_ptr<sensor_msgs::CameraInfo const> sharedCameraInfo = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(rgb_camera_info_topic, *nh, ros::Duration(5, 0));
      ROS_INFO("done");
      if (sharedCameraInfo != NULL) {
          cam_model=new image_geometry::PinholeCameraModel();
          cam_model->fromCameraInfo(*sharedCameraInfo);
      } else {
          ROS_WARN("Failed to get camera info");
      }
  }

  std::string orig_frame;
  pcl::PointCloud<point_t>::Ptr raw_point_cloud = getPointCloud(*nh, tfBuffer, pointcloud_topic, true/*false*/, &orig_frame);//TODO depth image instead of point cloud? Or uncolored points?
  if(raw_point_cloud==NULL || raw_point_cloud->points.size()==0) {
      ROS_ERROR("Failed to get a point cloud");
      return false;
  }
  ROS_INFO("Got point cloud, now continuing with getCloudROI");

  pcl::PointCloud<point_t>::Ptr point_cloud = getCloudROI(raw_point_cloud, 0.0, 3.0, -2.0, 2.0, 0.1, 2.0); // Remove ground
  try {
    pcl::io::savePCDFile ("/tmp/cloud_filtered_ROI.pcd", *point_cloud, true);
  } catch (pcl::IOException &e) {
    ROS_WARN("%s", e.what());
  }

  float shelfx, shelfy, shelfangle;
  if (searchForShelf(point_cloud, shelfx, shelfy, shelfangle)) { // TODO save cloud without shelf
    //if (shelfangle < -M_PI / 2.0 || shelfangle > M_PI / 2.0) {
    //  shelfangle = angleModulo(shelfangle + M_PI);
    //}
    ROS_INFO("Found shelf: x=%f, y=%f, angle=%f", shelfx, shelfy, shelfangle);
    addShelfToPlanningScene(shelfx, shelfy, shelfangle);
  } else {
    ROS_WARN("No shelf found, trying emergency solution");
    if (shelfx == 0.0)
      shelfx = 0.7;
    shelfy = 0.0;
    shelfangle = 0.0;
    addShelfToPlanningScene(shelfx, shelfy, shelfangle);
    // TODO just add the oobbs?
    ROS_WARN("findShelfService() finished, but no shelf found!");
    return false;
  }

  //pcl::PointCloud<point_t>::Ptr object_cloud(new pcl::PointCloud<point_t>());
  //pcl::transformPointCloud(*point_cloud, *object_cloud, isometry2affine(transform));
  //std::vector<vision_msgs::Detection3D> detections = getObjectsAndAddToPlanningScene(rgb_image, *cam_model, object_cloud, orig_frame, &transform, 0.0, true, false, NULL); // Here point_cloud must be in the original camera frame

  ROS_INFO("findShelfService() finished");
  return true;
}

bool findShelfAndObjectsService(
    storing_groceries_msgs::FindShelfAndObjects::Request &req,
    storing_groceries_msgs::FindShelfAndObjects::Response &res) {
  ROS_INFO_STREAM("findShelfAndObjectsService() called\nOptions: addToPlanningScene: "
                  << (req.addToPlanningScene ? "true" : "false"));
  if(cam_model == NULL) {// only do this once, since it does not change
      ROS_INFO("Getting camera info ... (5s timeout)");
      boost::shared_ptr<sensor_msgs::CameraInfo const> sharedCameraInfo = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(rgb_camera_info_topic, *nh, ros::Duration(5, 0));
      ROS_INFO("done");
      if (sharedCameraInfo != NULL) {
          cam_model=new image_geometry::PinholeCameraModel();
          cam_model->fromCameraInfo(*sharedCameraInfo);
      } else {
          ROS_ERROR("Failed to get camera info");
          return false;
      }
  }
  
  std_msgs::Header orig_header;
  Eigen::Isometry3d transform;
  sensor_msgs::Image rgb_image;
  pcl::PointCloud<point_t>::Ptr cloud=getSceneData(*nh, tfBuffer, cam_model, depth_image_topic, rgb_image_topic, orig_header, transform, rgb_image);
  // Needed after this point: cloud, orig_frame, transform, rgb_image
  pcl::PointCloud<point_t>::Ptr point_cloud;
  point_cloud = getCloudROI(cloud/*getPointCloud(*nh, tfBuffer, pointcloud_topic, true, &orig_frame)*/, 0.0, 3.0, -2.0, 2.0, 0.1, 2.0);//Remove ground
  if(point_cloud==NULL || point_cloud->points.size()==0) {
      ROS_ERROR("Failed to get a point cloud");
      return false;
  }
  
  float shelfx, shelfy, shelfangle;
  if (searchForShelf(point_cloud, shelfx, shelfy, shelfangle)) {
    //if (shelfangle < -M_PI / 2.0 || shelfangle > M_PI / 2.0) {
    //  shelfangle = angleModulo(shelfangle + M_PI);
    //}
    ROS_INFO("Found shelf: x=%f, y=%f, angle=%f", shelfx, shelfy, shelfangle);
    if (req.addToPlanningScene) {
      addShelfToPlanningScene(shelfx, shelfy, shelfangle);
    }
    res.shelf_x = shelfx;
    res.shelf_y = shelfy;
    res.shelf_angle = shelfangle;
    
    try {
      pcl::io::savePCDFile ("/tmp/cloud_wo_shelf.pcd", *point_cloud, true);
    } catch (pcl::IOException &e) {
      ROS_WARN("%s", e.what());
    }
    /*// Only consider the objects that are in the shelf TODO
    vision_msgs::BoundingBox3D inside_shelf_bbox;
    inside_shelf_bbox.center.position.x=shelfx;
    inside_shelf_bbox.center.position.y=shelfy;
    inside_shelf_bbox.center.position.z=;
    inside_shelf_bbox.center.orientation=;
    inside_shelf_bbox.size.z=;
    inside_shelf_bbox.size.x=;
    inside_shelf_bbox.size.y=;
    point_cloud=getCloudROI(point_cloud, inside_shelf_bbox);*/
  } else {
    ROS_WARN("No shelf found, trying emergency solution");
    if (shelfx == 0.0)
      shelfx = 0.7;
    shelfy = 0.0;
    shelfangle = 0.0;
    addShelfToPlanningScene(shelfx, shelfy, shelfangle);
    // TODO just add the oobbs?
    ROS_WARN("findShelfService() finished, but no shelf found!");
    return false;
  }
  
  pcl::PointCloud<point_t>::Ptr object_cloud(new pcl::PointCloud<point_t>());
  pcl::transformPointCloud(*point_cloud, *object_cloud, isometry2affine(transform));
  
  std::vector<vision_msgs::Detection3D> detections;
  if(useNewObjectMethod) {
      detections = getObjectsAndAddToPlanningScene(rgb_image, *cam_model, object_cloud, orig_header.frame_id, &transform, 0.0, req.addToPlanningScene, makeAllStandardCylinder, &res.objectIds); // Here point_cloud must be in the original camera frame
  } else {
      ROS_WARN("The option useNewObjectMethod is deprecated");
      detections = getObjectsAndAddToPlanningScene(rgb_image, *cam_model, object_cloud, orig_header.frame_id, &transform, 0.0, req.addToPlanningScene, makeAllStandardCylinder, &res.objectIds); // Here point_cloud must be in the original camera frame
  }

  res.detections = detections;
  vision_msgs::Detection3DArray det_msg;
  det_msg.header = orig_header;
  //det_msg.header.frame_id = orig_frame;
  //det_msg.header.stamp // TODO also set stamp?
  det_msg.detections = detections;
  detection_pub.publish(det_msg);
  /*for (size_t i = 0; i < res.detections.size(); i++) {
    res.detections[i].source_cloud.data.clear(); // This is too much data to print, maybe remove this later
  }*/
  ROS_INFO("findShelfAndObjectsService() finished");
  return true;
}

bool findObjectsInROIService(
    clf_grasping_msgs::FindObjectsInROI::Request &req,
    clf_grasping_msgs::FindObjectsInROI::Response &res) {
  ROS_INFO_STREAM("findObjectsInROIService() called\nOptions: roi:"
                  << req.roi << " addToPlanningScene: "
                  << (req.addToPlanningScene ? "true" : "false"));
  if(cam_model == NULL) {// only do this once, since it does not change
      ROS_INFO("Getting camera info ... (5s timeout)");
      boost::shared_ptr<sensor_msgs::CameraInfo const> sharedCameraInfo = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(rgb_camera_info_topic, *nh, ros::Duration(5, 0));
      ROS_INFO("done");
      if (sharedCameraInfo != NULL) {
          cam_model=new image_geometry::PinholeCameraModel();
          cam_model->fromCameraInfo(*sharedCameraInfo);
      } else {
          ROS_ERROR("Failed to get camera info");
          return false;
      }
  }
  
  std_msgs::Header orig_header;
  Eigen::Isometry3d transform;
  sensor_msgs::Image rgb_image;
  pcl::PointCloud<point_t>::Ptr cloud=getSceneData(*nh, tfBuffer, cam_model, depth_image_topic, rgb_image_topic, orig_header, transform, rgb_image);
  // Needed after this point: cloud, orig_frame, transform, rgb_image
  pcl::PointCloud<point_t>::Ptr point_cloud;
  if (req.roi.size.x == 0.0 && req.roi.size.y == 0.0 && req.roi.size.z == 0.0) { // if req.roi has sizes 0, then use a standard roi
    ROS_INFO("ROI volume is zero, using standard roi");
    point_cloud = getCloudROI(cloud/*getPointCloud(*nh, tfBuffer, pointcloud_topic, true, &orig_frame)*/, 0.0, 3.0, -2.0, 2.0, 0.1, 2.0);//Remove ground
  } else {
    point_cloud = getCloudROI(cloud/*getPointCloud(*nh, tfBuffer, pointcloud_topic, true, &orig_frame)*/, req.roi);
  }
  if(point_cloud==NULL || point_cloud->points.size()==0) {
      ROS_ERROR("Failed to get a point cloud");
      return false;
  }
  
  plane_t * table = searchAndAddTableToScene(point_cloud, orig_header.frame_id, NULL, true, false, makeTableToBox, tableSafetyMargin); // Here point_cloud must be in BASE_FRAME TODO why no cam model?
  
  float table_height;
  if(table!=NULL) {
      table_height=table->bbox.center.position.z;
      
      // TODO maybe only if req.roi has size 0?
      vision_msgs::BoundingBox3D above_table_bbox(table->bbox);
      above_table_bbox.center.position.z=(table_height+2.0)/2.0;
      above_table_bbox.size.z=2.0-table_height;
      above_table_bbox.size.x+=0.05; // For objects that are close to the edge of the table
      above_table_bbox.size.y+=0.05;
      try {
        pcl::io::savePCDFile ("/tmp/cloud_wo_table.pcd", *point_cloud, true);
      } catch (pcl::IOException &e) {
        ROS_WARN("%s", e.what());
      }
      point_cloud=getCloudROI(point_cloud, above_table_bbox); // Only consider the objects that are on the table // TODO error here?
      delete table;
  } else {
      table_height=0.0;
  }
  
  pcl::PointCloud<point_t>::Ptr object_cloud(new pcl::PointCloud<point_t>());
  pcl::transformPointCloud(*point_cloud, *object_cloud, isometry2affine(transform));
  
  std::vector<vision_msgs::Detection3D> detections;
  if(useNewObjectMethod) {
      detections = getObjectsAndAddToPlanningScene(rgb_image, *cam_model, object_cloud, orig_header.frame_id, &transform, table_height, req.addToPlanningScene, makeAllStandardCylinder, &res.objectIds); // Here point_cloud must be in the original camera frame
  } else {
      ROS_WARN("The option useNewObjectMethod is deprecated");
      detections = getObjectsAndAddToPlanningScene(rgb_image, *cam_model, object_cloud, orig_header.frame_id, &transform, table_height, req.addToPlanningScene, makeAllStandardCylinder, &res.objectIds); // Here point_cloud must be in the original camera frame
  }

  res.detections = detections;
  vision_msgs::Detection3DArray det_msg;
  det_msg.header = orig_header;
  //det_msg.header.frame_id = orig_frame;
  //det_msg.header.stamp // TODO also set stamp?
  det_msg.detections = detections;
  detection_pub.publish(det_msg);
  /*for (size_t i = 0; i < res.detections.size(); i++) {
    res.detections[i].source_cloud.data.clear(); // This is too much data to print, maybe remove this later
  }*/
  ROS_INFO("findObjectsInROIService() finished");
  return true;
}

bool learnObjectsService(
    //storing_groceries_msgs::LearnObjects::Request& req2,
    //storing_groceries_msgs::LearnObjects::Response&) {
    std_srvs::Empty::Request&,
    std_srvs::Empty::Response&) {
  ROS_INFO_STREAM("learnObjectsService() called");
  clf_grasping_msgs::FindObjectsInROI::Request req;//TODO fill req?
  req.roi = vision_msgs::BoundingBox3D();
  req.addToPlanningScene = false;
  clf_grasping_msgs::FindObjectsInROI::Response res;
  //findObjectsInROIService(req2, res);
  ROS_INFO_STREAM("findObjectsInROIService() called\nOptions: roi:"
                  << req.roi << " addToPlanningScene: "
                  << (req.addToPlanningScene ? "true" : "false"));
  
  ROS_INFO("Waiting for camera image and point cloud ...");
  //boost::shared_ptr<sensor_msgs::Image const> sharedRGBImage;
  boost::shared_ptr<sensor_msgs::Image const> sharedRGBImage = ros::topic::waitForMessage<sensor_msgs::Image>(rgb_image_topic, *nh, ros::Duration(10, 0));
  //boost::shared_ptr<sensor_msgs::PointCloud2 const> sharedCloud;
  //ros::topic::waitForMessages<sensor_msgs::Image, sensor_msgs::PointCloud2>(rgb_image_topic, pointcloud_topic, *nh, ros::Duration(10, 0), sharedRGBImage, sharedCloud);
  sensor_msgs::Image depth_image=averageImages(*nh, "/xtion/depth_registered/image_raw", 20, 0.1);
  ROS_INFO("done");
  if(cam_model == NULL) {// only do this once, since it does not change
      ROS_INFO("Getting camera info ... (5s timeout)");
      boost::shared_ptr<sensor_msgs::CameraInfo const> sharedCameraInfo = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(rgb_camera_info_topic, *nh, ros::Duration(5, 0));
      ROS_INFO("done");
      if (sharedCameraInfo != NULL) {
          cam_model=new image_geometry::PinholeCameraModel();
          cam_model->fromCameraInfo(*sharedCameraInfo);
      } else {
          ROS_ERROR("Failed to get camera info");
          return false;
      }
  }
  
  /*if (sharedCloud == NULL) {
    ROS_ERROR("Failed to get point cloud");
    return false;
  }*/
  if (sharedRGBImage == NULL) {
    ROS_ERROR("Failed to get camera image");
    return false;
  }
  pcl::PointCloud<point_t>::Ptr cloud = depthImageToPointcloud(depth_image, &(*sharedRGBImage), *cam_model); // Regarding the second parameter: is a boost shared pointer, should be a normal pointer
  Eigen::Isometry3d transform=getIsometryTransform(tfBuffer, depth_image.header.frame_id, BASE_FRAME);
  pcl::transformPointCloud(*cloud, *cloud, isometry2affine(transform.inverse()));
  // Make unorganized
  cloud->width = (cloud->width) * (cloud->height);
  cloud->height = 1;
  
  //ROS_INFO("Getting camera image");
  ROS_INFO_STREAM("Got camera image with stamp " << sharedRGBImage->header.stamp);
  sensor_msgs::Image rgb_image = *sharedRGBImage;
  // Save image (e.g. for logging/introspection purposes)
  uint32_t time = ros::WallTime::now().sec;
  std::ostringstream imgstreamtmp;
  imgstreamtmp << "/tmp/" << time << ".jpg";
  cv::imwrite(imgstreamtmp.str(), cv_bridge::toCvShare(boost::make_shared<sensor_msgs::Image>(rgb_image), sensor_msgs::image_encodings::BGR8)->image);

  std::string orig_frame = depth_image.header.frame_id;
  pcl::PointCloud<point_t>::Ptr point_cloud;
  if (req.roi.size.x == 0.0 && req.roi.size.y == 0.0 && req.roi.size.z == 0.0) { // if req.roi has sizes 0, then use a standard roi
    ROS_INFO("ROI volume is zero, using standard roi");
    point_cloud = getCloudROI(cloud/*getPointCloud(*nh, tfBuffer, pointcloud_topic, true, &orig_frame)*/, 0.0, 3.0, -2.0, 2.0, 0.1, 2.0);//Remove ground
  } else {
    point_cloud = getCloudROI(cloud/*getPointCloud(*nh, tfBuffer, pointcloud_topic, true, &orig_frame)*/, req.roi);
  }
  if(point_cloud==NULL || point_cloud->points.size()==0) {
      ROS_ERROR("Failed to get a point cloud");
      return false;
  }
  
  plane_t * table = searchAndAddTableToScene(point_cloud, orig_frame, NULL, true, false, makeTableToBox, tableSafetyMargin); // Here point_cloud must be in BASE_FRAME
  
  float table_height;
  if(table!=NULL) {
      table_height=table->bbox.center.position.z;
      
      // TODO maybe only if req.roi has size 0?
      vision_msgs::BoundingBox3D above_table_bbox(table->bbox);
      above_table_bbox.center.position.z=(table_height+2.0)/2.0;
      above_table_bbox.size.z=2.0-table_height;
      above_table_bbox.size.x+=0.05; // For objects that are close to the edge of the table
      above_table_bbox.size.y+=0.05;
      point_cloud=getCloudROI(point_cloud, above_table_bbox); // Only consider the objects that are on the table
      delete table;
  } else {
      table_height=0.0;
  }
  
  pcl::PointCloud<point_t>::Ptr object_cloud(new pcl::PointCloud<point_t>());
  pcl::transformPointCloud(*point_cloud, *object_cloud, isometry2affine(transform));
  
  std::vector<vision_msgs::Detection3D> detections;
  if(useNewObjectMethod) {
      detections = getObjectsAndAddToPlanningScene(rgb_image, *cam_model, object_cloud, orig_frame, &transform, table_height, req.addToPlanningScene, makeAllStandardCylinder, &res.objectIds); // Here point_cloud must be in the original camera frame
  } else {
      ROS_WARN("The option useNewObjectMethod is deprecated");
      detections = getObjectsAndAddToPlanningScene(rgb_image, *cam_model, object_cloud, orig_frame, &transform, table_height, req.addToPlanningScene, makeAllStandardCylinder, &res.objectIds); // Here point_cloud must be in the original camera frame
  }

  res.detections = detections;
  vision_msgs::Detection3DArray det_msg;
  det_msg.header = depth_image.header;
  //det_msg.header.frame_id = orig_frame;
  //det_msg.header.stamp // TODO also set stamp?
  det_msg.detections = detections;
  detection_pub.publish(det_msg);
  /*for (size_t i = 0; i < res.detections.size(); i++) {
    res.detections[i].source_cloud.data.clear(); // This is too much data to print, maybe remove this later
  }*/
  ROS_INFO("findObjectsInROIService() finished");
  
  
  
  
  //uint32_t time = ros::WallTime::now().sec;
  //TODO test if learnObjectsPath ends with a "/"?
  //TODO create folders if not existant?
  std::ostringstream imgstream;
  imgstream << learnObjectsPath << "images/" << time << ".jpg";
  ROS_INFO("Saving the image was: %s", (cv::imwrite(imgstream.str(), cv_bridge::toCvShare(boost::make_shared<sensor_msgs::Image>(rgb_image), sensor_msgs::image_encodings::BGR8)->image)?"successful":"unsuccessful"));
  
  std::ofstream label_file;
  std::ostringstream txtstream;
  txtstream << learnObjectsPath << "labels/" << time << ".txt";
  label_file.open (txtstream.str());
  ROS_INFO("labels file is open:%s", (label_file.is_open()?"true":"false"));
  float xmin, xmax, ymin, ymax;
  //subimageBoundaries(tf2::Vector3(res.detections[0].bbox.center.position.x, res.detections[0].bbox.center.position.y, res.detections[0].bbox.center.position.z), tf2::Quaternion(res.detections[0].bbox.center.orientation.x, res.detections[0].bbox.center.orientation.y, res.detections[0].bbox.center.orientation.z, res.detections[0].bbox.center.orientation.w), tf2::Vector3(res.detections[0].bbox.size.x, res.detections[0].bbox.size.y, res.detections[0].bbox.size.z), *cam_model, xmin, ymin, xmax, ymax);
  pcl::PointCloud<point_t>::Ptr detection_cloud(new pcl::PointCloud<point_t>);
  pcl::fromROSMsg(res.detections[0].source_cloud, *detection_cloud);
  subimageBoundaries(detection_cloud, *cam_model, xmin, ymin, xmax, ymax);
  label_file << /*req.data[0]*/0 << " " << (xmin+xmax)/2.0/rgb_image.width << " " << (ymin+ymax)/2.0/rgb_image.height << " " << (xmax-xmin)/rgb_image.width << " " << (ymax-ymin)/rgb_image.height << "\n"; // TODO selectable id
  label_file.close();
  
  std::ostringstream cloudstream;
  cloudstream << learnObjectsPath << "clouds/" << /*req.data[0]*/0 << "/" << time << ".pcd";
  pcl::PointCloud<point_t>::Ptr output_cloud(new pcl::PointCloud<point_t>);
  pcl::fromROSMsg(res.detections[0].source_cloud, *output_cloud);
  // TODO move to origin?
  try {
    pcl::io::savePCDFile (cloudstream.str(), *output_cloud, true);
  } catch (pcl::IOException &e) {
    ROS_WARN("%s", e.what());
  }
  return true;
}

bool computeTargetRectangleService(
    storing_groceries_msgs::GetPlaceRectangle::Request& req,
    storing_groceries_msgs::GetPlaceRectangle::Response& res) {
  double angle;
  moveit_msgs::CollisionObject objectInGripper;
  std::string support_surface = computeTargetPosition(req.name, res.center.pose.position.x, res.center.pose.position.y,
                                  res.center.pose.position.z, res.size_x, res.size_y,
                                  angle, req.shelf_x, req.shelf_y,
                                  req.shelf_angle, &objectInGripper);
  // TODO angle to res.center.pose.orientation
  
  ROS_INFO_STREAM("objectInGripper=" << objectInGripper);
  std::vector<moveit_msgs::PlaceLocation> places =
      generatePlaces(objectInGripper, res.center.pose.position.x, res.center.pose.position.y, res.center.pose.position.z, res.size_x, res.size_y, angle);
  return true;
}

bool attachTestObjectService(
    std_srvs::Empty::Request& req,
    std_srvs::Empty::Response& res) {
  moveit_msgs::AttachedCollisionObject aco;
  // TODO fill aco
  aco.link_name = "arm_tool_link";
  aco.object.header.frame_id = "arm_tool_link";
  aco.object.id = "pringles0";
  aco.object.primitives.resize(1);
  aco.object.primitive_poses.resize(1);
  
  //aco.object.primitives[0].type = aco.object.primitives[0].CYLINDER;
  //aco.object.primitives[0].dimensions.resize(2);
  //aco.object.primitives[0].dimensions[0] = 0.22;
  //aco.object.primitives[0].dimensions[1] = 0.04;
  
  aco.object.primitives[0].type = aco.object.primitives[0].BOX;
  aco.object.primitives[0].dimensions.resize(3);
  aco.object.primitives[0].dimensions[0] = 0.05;
  aco.object.primitives[0].dimensions[1] = 0.15;
  aco.object.primitives[0].dimensions[2] = 0.25;
  
  //aco.object.primitives[0].type = aco.object.primitives[0].SPHERE;
  //aco.object.primitives[0].dimensions.resize(1);
  //aco.object.primitives[0].dimensions[0] = 0.04;
  
  aco.object.primitive_poses[0].position.x = 0.19;
  aco.object.primitive_poses[0].position.y = 0.0;
  aco.object.primitive_poses[0].position.z = 0.0;
  aco.object.primitive_poses[0].orientation.x = -0.5;
  aco.object.primitive_poses[0].orientation.y = 0.5;
  aco.object.primitive_poses[0].orientation.z = 0.5;
  aco.object.primitive_poses[0].orientation.w = 0.5;
  //aco.touch_links = ;
  //aco.detach_posture = ;
  //aco.weight = ;
  ROS_INFO_STREAM("Attaching object:" << std::endl << aco);
  applyAttachedCollisionObject(aco);
  return true;
}

// TODO unsuccessful returns
// prerequisites/preconditions: Object in gripper, lift down, arm close to torso
// TODO
// postconditions: No object in gripper, lift down, arm close to torso TODO
void placeObjectOnRectangleCB(
    const storing_groceries_msgs::PlaceOnRectangleGoalConstPtr & goal) {
  ROS_INFO_STREAM("placeObjectOnRectangleCB called, goal->support=" << goal->support);
  storing_groceries_msgs::PlaceOnRectangleResult result;
  storing_groceries_msgs::PlaceOnRectangleFeedback feedback;

  std::map<std::string, moveit_msgs::AttachedCollisionObject> attachedObjects = getAttachedObjects();
  ROS_INFO_STREAM("Number of attached objects: " << attachedObjects.size());
  moveit_msgs::CollisionObject objectInGripper;
  if (attachedObjects.size() > 0) {
    objectInGripper = attachedObjects.cbegin()->second.object;
    ROS_INFO_STREAM("objectInGripper=" << attachedObjects.cbegin()->second);
    if (attachedObjects.size() > 1) {
      ROS_WARN("There are %lu objects attached to the robot, should only be one!", attachedObjects.size());
    }
  } else {
    ROS_WARN("There no objects attached to the robot!");
    result.result_code = 99999; // FAILURE
    place_as->setAborted(result, "no object attached");
    return;
  }

  const double angle = 2.0 * acos((goal->center.pose.orientation.z < 0.0 ? -1.0 : 1.0) *  goal->center.pose.orientation.w / sqrt(std::pow(goal->center.pose.orientation.w, 2) + std::pow(goal->center.pose.orientation.z, 2))); // TODO what if w and z are zero? Is that possible? TODO use yaw_from_quat instead?
#ifdef SIMULATION
  bool simulation=true;
#else
  bool simulation=false;
#endif
  std::vector<moveit_msgs::PlaceLocation> places =
      generatePlaces(objectInGripper, goal->center.pose.position.x, goal->center.pose.position.y, goal->center.pose.position.z, goal->size_x, goal->size_y, angle, simulation);
  ROS_INFO("generatePlaces() gave %lu place locations", places.size());
  if (places.size() == 0) {
    result.result_code = 99999; // FAILURE
    place_as->setAborted(result, "no place found");
    return;
  }

  feedback.state = "generated places";
  place_as->publishFeedback(feedback);

  if (place_as->isPreemptRequested() || !ros::ok()) {
    ROS_INFO("Preempted");
    result.result_code = -7; // PREEMPTED
    place_as->setPreempted(result, "preempted"); // set the action state to preempted
    return;
  }

  ROS_INFO("Calling place ...");
  moveit::planning_interface::MoveItErrorCode moveit_result;
  const bool plan_only = false; // true=plan only, false=also execute
  const bool place_eef = false;
  bool allow_gripper_support_collision = false;
  group->setSupportSurfaceName(goal->support);
  //moveit_result = group->place(objectInGripper.id, places, plan_only);
  moveit_result = my_place(group, objectInGripper.id, places, goal->support, allow_gripper_support_collision,place_eef, plan_only);

  if (!moveit_result) {
    ROS_INFO("First try failed, trying again with support surface collision allowed ...");
    allow_gripper_support_collision = true;
    //moveit_result = group->place(objectInGripper.id, places, plan_only);
    moveit_result = my_place(group, objectInGripper.id, places, goal->support, allow_gripper_support_collision, place_eef, plan_only);
  }
  group->setSupportSurfaceName("");
  ROS_INFO("done");

  if (!moveit_result) {
    ROS_ERROR("place failed with code %i", moveit_result.val);
    result.result_code = moveit_result.val;
    place_as->setAborted(result, "place failed");
    return;
  }

  result.result_code = 1; // SUCCESS
  place_as->setSucceeded(result, "success");
  ROS_INFO("Place finished successfully");
}

/*
bool findCutleryAndDishesService(
    std_srvs::Empty::Request& req,
    std_srvs::Empty::Response& res) {
    //Goal: Cutlery: bbox on table. Dishes: 3D model (2 circles)
    //find table
    cv::Point * hull=malloc(table.hull.size() * sizeof(cv::Point));
    for(size_t i=0; i<table.hull.size(); ++i) {
        cv::Point2d point=project3dToPixel(cv::Point3d(table.hull[i].x, table.hull[i].y, table.hull[i].z));
        hull[i] = cv::Point(std::round(point.x), std::round(point.y));//TODO check if in image bounds?
    }
    cv::Mat table_mask;
    cv::fillConvexPoly(table_mask, points, table.hull.size(), cv::Scalar(1.0, 1.0, 1.0), cv::LINE_8, 0);
    //do segmentation (considering table_mask)
    pcl::PointCloud<point_t>::Ptr object_points();
    for() {
        cv::Point3d ray=projectPixelTo3dRay(cv::Point2d(, ));
        double ix, iy, iz;
        if(intersectionPointOfLineAndPlane(, , , , ray.x, ray.y, ray.z, ix, iy, iz) {}
    }
    vision_msgs::BoundingBox3D bbox=minimum_bbox_of_plane(object_points, const pcl::Vertices& polygon, const shape_msgs::Plane& plane); // TODO or find bbox in image and project that? is it still a box then?
    //test if dimensions of bbox are right for cutlery
}
*/

int main(int argc, char **argv) {
  ROS_INFO("Starting node ...");
  ros::init(argc, argv, "grasping_object_recognition");
  ros::NodeHandle new_handle("~");
  nh = &new_handle;
  
  //ROS_INFO("Getting params ...");
  //nh->param<std::string>("pointcloud_topic", pointcloud_topic, "/xtion/depth_registered/points");
  //nh->param<std::string>("rgb_image_topic", rgb_image_topic, "/xtion/rgb/image_raw");
  //ROS_INFO_STREAM("Params are: pointcloud_topic=" << pointcloud_topic << ", rgb_image_topic=" << rgb_image_topic);
  //std::string objects_yaml_path;
  //nh->getParam("objects_config_path", objects_yaml_path);
  //ROS_INFO_STREAM("Got objects_config_path:>" << objects_yaml_path << "<");
  //parseConfigYAML(objects_yaml_path);
  
  tf2_ros::TransformListener newListener(tfBuffer);
  tfListener = &newListener;

  ROS_INFO("Initializing sub-modules ...");
  init_high_level(nh, &tfBuffer);
  init_plane_publisher(*nh, "/planes");
  init_grasp_publisher(*nh);
  initActionClients(*nh);
  detection_pub = nh->advertise<vision_msgs::Detection3DArray>("/object_detections", 100);

  dynamic_reconfigure::Server<grasping_object_recognition::dyn_reconfConfig> server;
  dynamic_reconfigure::Server<grasping_object_recognition::dyn_reconfConfig>::CallbackType f;
  f = boost::bind(&reconfigure_callback, _1, _2);
  server.setCallback(f);
  
  ROS_INFO("Getting camera info ... (5s timeout)");// only do this once, since it does not change
  boost::shared_ptr<sensor_msgs::CameraInfo const> sharedCameraInfo = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(rgb_camera_info_topic, *nh, ros::Duration(5, 0));
  ROS_INFO("done");
  if (sharedCameraInfo != NULL) {
      cam_model=new image_geometry::PinholeCameraModel();
      cam_model->fromCameraInfo(*sharedCameraInfo);
  } else {
      ROS_WARN("Failed to get camera info");
  }

  moveit::planning_interface::MoveGroupInterface new_group("arm_torso"); // can be arm or arm_torso
  group = &new_group;

  ROS_INFO("Advertising services ...");
  ros::ServiceServer tableService =
      nh->advertiseService("findTable", findTableService);
  ros::ServiceServer shelfService =
      nh->advertiseService("findShelf", findShelfService);
  ros::ServiceServer shelfAndObjectsService =
      nh->advertiseService("findShelfAndObjects", findShelfAndObjectsService);
  ros::ServiceServer objectsService =
      nh->advertiseService("findObjects", findObjectsInROIService);
  ros::ServiceServer learnService =
      nh->advertiseService("learnObjects", learnObjectsService);
  ros::ServiceServer targetService =
      nh->advertiseService("computeTarget", computeTargetRectangleService);
  ros::ServiceServer attachTestService =
      nh->advertiseService("attachTestObject", attachTestObjectService);
  
  actionlib::SimpleActionServer<storing_groceries_msgs::PlaceOnRectangleAction>
      new_place_as(*nh, "placeObject", boost::bind(&placeObjectOnRectangleCB, _1),
                   false);
  place_as = &new_place_as;
  place_as->start();

  ROS_INFO("Services are running, node is reading, spinning ...");

  ros::spin(); // For the action servers
  return 0;
}
