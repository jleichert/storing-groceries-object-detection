/**
 * @file
 * @author  Markus Vieth
 * @section DESCRIPTION
 *
 * Point cloud functionality
 */
#pragma once
#include <vision_msgs/BoundingBox3D.h>
#include <Eigen/Geometry> // for Vector3f, Quaternionf
#include <vector>
#include <pcl/point_cloud.h>
#include "point_clouds.hpp" // for point_t

namespace OOBB_Calculate {
    //Calculate an object-oriented bounding box using a pca
    //TODO guarantee that sizes are ordered?
    vision_msgs::BoundingBox3D calculate(const pcl::PointCloud<point_t>::ConstPtr temp_cloud);
    
    //Calculate an object-oriented bounding box using pcl::MomentOfInertiaEstimation. Rather slow.
    //TODO guarantee that sizes are ordered?
    vision_msgs::BoundingBox3D calculate2(const pcl::PointCloud<point_t>::ConstPtr temp_cloud);
    
    //Calculate the axis-aligned bounding box. Rather slow.
    //TODO guarantee that sizes are ordered?
    vision_msgs::BoundingBox3D calculateAABB(const pcl::PointCloud<point_t>::ConstPtr temp_cloud);
    
    // Order the values in size so that they are ascending or descending (depending on the parameter asc). At the same time, adapt orientation so that the bounding box stays the same.
    void orderSizeAndAdaptOrientation(Eigen::Vector3f& size, Eigen::Quaternionf& orientation, bool asc);
    
    //Calculate an oobb and an aabb using pcl::MomentOfInertiaEstimation, then choose the smallest
    //TODO guarantee that sizes are ordered?
    vision_msgs::BoundingBox3D chooseBest3(const pcl::PointCloud<point_t>::ConstPtr temp_cloud);
    
    //Calculate two oobbs and one aabb by calling calculate(), calculate2(), and calculateAABB(), then choose the smallest. calculate() and calculate2() give very similar results
    //TODO guarantee that sizes are ordered?
    vision_msgs::BoundingBox3D chooseBest2(pcl::PointCloud<point_t>::ConstPtr temp_cloud);
    
    // usually (always?), the oobb is better than the aabb, so we don't need to calculate the aabb
    vision_msgs::BoundingBox3D chooseBest(pcl::PointCloud<point_t>::ConstPtr temp_cloud);
}

// center x, center y, rotation, width, height
std::vector<float> minimum_bounding_box2d(const pcl::PointCloud<point_t>::ConstPtr points, const pcl::Vertices& polygon);

// center x, center y, rotation, width, height
// probably slower than the other function
std::vector<float> minimum_bounding_box2d_v2(const pcl::PointCloud<point_t>::ConstPtr points, const pcl::Vertices& polygon);

/** Compute the 2D minimum bounding box of a point cloud. Find the ConvexHull first, which produces points and polygon
 */
vision_msgs::BoundingBox3D minimum_bbox_of_plane(const pcl::PointCloud<point_t>::ConstPtr points, const pcl::Vertices& polygon, const shape_msgs::Plane& plane);
