/* Author: Markus Vieth*/
#include <memory>
#include <time.h>

// ros and libraries
#include <Eigen/Geometry>
#include <actionlib/server/simple_action_server.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl_ros/transforms.h>
#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>

// Messages
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <control_msgs/JointTrajectoryControllerState.h>
#include <eigen_conversions/eigen_msg.h>
#include <storing_groceries_msgs/GroceriesAction.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#include "high_level_functions.hpp"
#include "object_knowledge.hpp"
#include "pick_place.hpp"
#include "point_clouds.hpp"
#include "util.hpp"

/*int main(int argc, char **argv) {
  ros::init(argc, argv, "tests_node");
  ros::NodeHandle nh("~");
  tf2_ros::Buffer tfBuffer(ros::Duration(15.0));
  tf2_ros::TransformListener tfListener(tfBuffer);
  
  ros::Duration(3).sleep();
  
  std::string frame;
  pcl::PointCloud<point_t>::Ptr point_cloud = getPointCloud(&nh, tfBuffer, "/xtion/depth_registered/points", true, &frame);
  ROS_INFO_STREAM("Frame: " << frame);
  try {
      pcl::io::savePCDFile ("point_cloud.pcd", *point_cloud, true);
  } catch(pcl::IOException &e) {
      ROS_WARN("%s", e.what());
  }
  
  //moveit::planning_interface::PlanningSceneInterface psi;
  //std::vector<moveit_msgs::CollisionObject> coll_objects;
  //auto rand_obj=addRandomCollisionObject(3);
  //coll_objects.push_back(rand_obj);
  
  //plane_t plane;
  //plane.plane.coef[2] = 1.0;
  //plane.plane.coef[3] = -0.68;
  //moveit_msgs::CollisionObject collision_object(rand_obj);
  //collision_object.id = "corrected";
  //primitive_stability_wrapper(plane, Eigen::Vector3f(0.0, 0.0, -1.0), collision_object);
  
  //coll_objects.push_back(collision_object);
  //ROS_INFO_STREAM("adding new collision object: " << collision_object);
  //psi.applyCollisionObjects(coll_objects);
  
  ros::spin();
  return 0;
}*/
/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2010, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id$
 *
 */

/* \author Radu Bogdan Rusu
 * adaptation Raphael Favier*/

#include <boost/make_shared.hpp>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/point_representation.h>

#include <pcl/io/pcd_io.h>

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/filter.h>
#include <pcl/segmentation/extract_clusters.h>

#include <pcl/features/normal_3d.h>

#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/gicp.h>
#include <pcl/registration/transforms.h>

#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/point_cloud_color_handlers.h>

//convenient typedefs
typedef pcl::PointXYZRGBA PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef pcl::PointXYZRGBA PointNormalT;
typedef pcl::PointCloud<PointNormalT> PointCloudWithNormals;

// This is a tutorial so we can afford having global variables 
//our visualizer
pcl::visualization::PCLVisualizer *p;
//its left and right viewports
int vp_1, vp_2;

//convenient structure to handle our pointclouds
struct PCD
{
  PointCloud::Ptr cloud;
  std::string f_name;

  PCD() : cloud (new PointCloud) {};
};

struct PCDComparator
{
  bool operator () (const PCD& p1, const PCD& p2)
  {
    return (p1.f_name < p2.f_name);
  }
};


// Define a new point representation for < x, y, z, curvature >
class MyPointRepresentation : public pcl::PointRepresentation <PointNormalT>
{
  using pcl::PointRepresentation<PointNormalT>::nr_dimensions_;
public:
  MyPointRepresentation ()
  {
    // Define the number of dimensions
    nr_dimensions_ = 6;
  }

  // Override the copyToFloatArray method to define our feature vector
  virtual void copyToFloatArray (const PointNormalT &p, float * out) const
  {
    // < x, y, z, curvature >
    out[0] = p.x;
    out[1] = p.y;
    out[2] = p.z;
    out[3] = p.r;
    out[4] = p.g;
    out[5] = p.b;
  }
};


////////////////////////////////////////////////////////////////////////////////
/** \brief Display source and target on the first viewport of the visualizer
 *
 */
void showCloudsLeft(const PointCloud::Ptr cloud_target, const PointCloud::Ptr cloud_source)
{
  p->removePointCloud ("vp1_target");
  p->removePointCloud ("vp1_source");

  pcl::visualization::PointCloudColorHandlerRGBField<PointT> tgt_h (cloud_target);
  pcl::visualization::PointCloudColorHandlerRGBField<PointT> src_h (cloud_source);
  p->addPointCloud (cloud_target, tgt_h, "vp1_target", vp_1);
  p->addPointCloud (cloud_source, src_h, "vp1_source", vp_1);

  PCL_INFO ("Press q to begin the registration.\n");
  p-> spin();
}


////////////////////////////////////////////////////////////////////////////////
/** \brief Display source and target on the second viewport of the visualizer
 *
 */
void showCloudsRight(const PointCloudWithNormals::Ptr cloud_target, const PointCloudWithNormals::Ptr cloud_source)
{
  p->removePointCloud ("source");
  p->removePointCloud ("target");


  //PointCloudColorHandlerGenericField<PointNormalT> tgt_color_handler (cloud_target, "curvature");
  pcl::visualization::PointCloudColorHandlerRGBField<PointT> tgt_color_handler (cloud_target);
  if (!tgt_color_handler.isCapable ())
      PCL_WARN ("Cannot create curvature color handler!");

  //PointCloudColorHandlerGenericField<PointNormalT> src_color_handler (cloud_source, "curvature");
  pcl::visualization::PointCloudColorHandlerRGBField<PointT> src_color_handler (cloud_source);
  if (!src_color_handler.isCapable ())
      PCL_WARN ("Cannot create curvature color handler!");


  p->addPointCloud (cloud_target, tgt_color_handler, "target", vp_2);
  p->addPointCloud (cloud_source, src_color_handler, "source", vp_2);

  p->spinOnce();
}

////////////////////////////////////////////////////////////////////////////////
/** \brief Load a set of PCD files that we want to register together
  * \param argc the number of arguments (pass from main ())
  * \param argv the actual command line arguments (pass from main ())
  * \param models the resultant vector of point cloud datasets
  */
void loadData (int argc, char **argv, std::vector<PCD, Eigen::aligned_allocator<PCD> > &models)
{
  std::string extension (".pcd");
  // Suppose the first argument is the actual test model
  for (int i = 1; i < argc; i++)
  {
    std::string fname = std::string (argv[i]);
    // Needs to be at least 5: .plot
    if (fname.size () <= extension.size ())
      continue;

    std::transform (fname.begin (), fname.end (), fname.begin (), (int(*)(int))tolower);

    //check that the argument is a pcd file
    if (fname.compare (fname.size () - extension.size (), extension.size (), extension) == 0)
    {
      // Load the cloud and saves it into the global list of models
      PCD m;
      m.f_name = argv[i];
      pcl::io::loadPCDFile (argv[i], *m.cloud);
      //remove NAN points from the cloud
      std::vector<int> indices;
      pcl::removeNaNFromPointCloud(*m.cloud,*m.cloud, indices);

      models.push_back (m);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
/** \brief Align a pair of PointCloud datasets and return the result
  * \param cloud_src the source PointCloud
  * \param cloud_tgt the target PointCloud
  * \param output the resultant aligned source PointCloud
  * \param final_transform the resultant transform between source and target
  */
void pairAlign (const PointCloud::Ptr cloud_src, const PointCloud::Ptr cloud_tgt, PointCloud::Ptr output, Eigen::Matrix4f &final_transform/*, bool downsample = false*/)
{
  //
  // Downsample for consistency and speed
  // \note enable this for large datasets
  PointCloud::Ptr src (new PointCloud);
  PointCloud::Ptr tgt (new PointCloud);
  /*pcl::VoxelGrid<PointT> grid;
  if (downsample)
  {
    grid.setLeafSize (0.05, 0.05, 0.05);
    grid.setInputCloud (cloud_src);
    grid.filter (*src);

    grid.setInputCloud (cloud_tgt);
    grid.filter (*tgt);
  }
  else
  {*/
    src = cloud_src;
    tgt = cloud_tgt;
  //}


  // Compute surface normals and curvature
  //PointCloudWithNormals::Ptr points_with_normals_src (new PointCloudWithNormals);
  //PointCloudWithNormals::Ptr points_with_normals_tgt (new PointCloudWithNormals);

  /*pcl::NormalEstimation<PointT, PointNormalT> norm_est;
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
  norm_est.setSearchMethod (tree);
  norm_est.setKSearch (30);
  
  norm_est.setInputCloud (src);
  norm_est.compute (*points_with_normals_src);
  pcl::copyPointCloud (*src, *points_with_normals_src);

  norm_est.setInputCloud (tgt);
  norm_est.compute (*points_with_normals_tgt);
  pcl::copyPointCloud (*tgt, *points_with_normals_tgt);*/

  //
  // Instantiate our custom point representation (defined above) ...
  MyPointRepresentation point_representation;
  // ... and weight the 'curvature' dimension so that it is balanced against x, y, and z
  float alpha[6] = {1.0, 1.0, 1.0, 0.03, 0.03, 0.03};
  point_representation.setRescaleValues (alpha);

  //
  // Align
  pcl::GeneralizedIterativeClosestPoint<PointNormalT, PointNormalT> reg;
  reg.setTransformationEpsilon (1e-8);
  // Set the maximum distance between two correspondences (src<->tgt) to 10cm
  // Note: adjust this based on the size of your datasets
  reg.setMaxCorrespondenceDistance (0.05);  
  // Set the point representation
  reg.setPointRepresentation (boost::make_shared<const MyPointRepresentation> (point_representation));

  reg.setInputSource (src);
  reg.setInputTarget (tgt);



  //
  // Run the same optimization in a loop and visualize the results
  Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity (), prev, targetToSource;
  PointCloudWithNormals::Ptr reg_result = src;
  reg.setMaximumIterations (10);
  for (int i = 0; i < 100; ++i)
  {
    PCL_INFO ("Iteration Nr. %d.\n", i);

    // save cloud for visualization purpose
    src = reg_result;

    // Estimate
    reg.setInputSource (src);
    reg.align (*reg_result);

    //accumulate transformation between each Iteration
    Ti = reg.getFinalTransformation () * Ti;
    std::cout << "Final transformation: " << reg.getFinalTransformation () << std::endl;

    //if the difference between this transformation and the previous one
    //is smaller than the threshold, refine the process by reducing
    //the maximal correspondence distance
    //if (fabs ((reg.getLastIncrementalTransformation () - prev).sum ()) < reg.getTransformationEpsilon ())
      //reg.setMaxCorrespondenceDistance (reg.getMaxCorrespondenceDistance () - 0.001);
      reg.setMaxCorrespondenceDistance (std::max(reg.getMaxCorrespondenceDistance () * 0.98, 0.005));
    std::cout << "Correspondence distance is now " << reg.getMaxCorrespondenceDistance () << std::endl;
    std::cout << "Fitness score=" << reg.getFitnessScore(0.1) << std::endl;
    prev = reg.getLastIncrementalTransformation ();

    // visualize current state
    showCloudsRight(tgt, src);
  }

  // Get the transformation from target to source
  targetToSource = Ti.inverse();


  // Transform target back in source frame
  //pcl::transformPointCloud (*cloud_src, *output, Ti);
   *output = *tgt;

  //p->removePointCloud ("source");
  //p->removePointCloud ("target");

  //pcl::visualization::PointCloudColorHandlerRGBField<PointT>  cloud_tgt_h (output);
  //pcl::visualization::PointCloudColorHandlerRGBField<PointT>  cloud_src_h (cloud_src);
  //p->addPointCloud (output, cloud_tgt_h, "target", vp_2);
  //p->addPointCloud (cloud_src, cloud_src_h, "source", vp_2);

  PCL_INFO ("Press q to continue the registration.\n");
  p->spin ();

  p->removePointCloud ("source"); 
  p->removePointCloud ("target");

  //add the source to the transformed target
  *output += *src;
  
  final_transform = targetToSource;
 }

unsigned int text_id = 0;
void keyboardEventOccurred (const pcl::visualization::KeyboardEvent &event,
                            void* viewer_void)
{
  pcl::visualization::PCLVisualizer *viewer = static_cast<pcl::visualization::PCLVisualizer *> (viewer_void);
  if (event.getKeySym () == "q" && event.keyDown ())
  {
    viewer->close();
  }
}

int main (int argc, char** argv) {
    if(argc<2) {
        printf("Not enough arguments");
        exit(EXIT_SUCCESS);
    }
    ros::init(argc, argv, "tests_node");
    ros::NodeHandle nh("~");
    tf2_ros::Buffer tfBuffer(ros::Duration(15.0));
    tf2_ros::TransformListener tfListener(tfBuffer);
  
    ros::Duration(1).sleep();
  
    sensor_msgs::Image depth_image;
    sensor_msgs::Image rgb_image;
    /*boost::shared_ptr<sensor_msgs::Image const> sharedImage1 = ros::topic::waitForMessage<sensor_msgs::Image>("/xtion/depth/image", nh, ros::Duration(10, 0));
    if (sharedImage1 != NULL) {
        ROS_INFO("Got camera image");
        depth_image = *sharedImage1;
    }*/
    depth_image=medianImages(nh, "/xtion/depth_registered/image_raw", 20, 0.1);
    boost::shared_ptr<sensor_msgs::Image const> sharedImage2 = ros::topic::waitForMessage<sensor_msgs::Image>("/xtion/rgb/image_raw", nh, ros::Duration(10, 0));
    if (sharedImage2 != NULL) {
        ROS_INFO("Got camera image");
        rgb_image = *sharedImage2;
    }
    image_geometry::PinholeCameraModel cam_model;
    boost::shared_ptr<sensor_msgs::CameraInfo const> sharedCameraInfo = ros::topic::waitForMessage<sensor_msgs::CameraInfo>("/xtion/rgb/camera_info", nh, ros::Duration(10, 0));
    if (sharedCameraInfo != NULL) {
        ROS_INFO("Got camera model");
        cam_model.fromCameraInfo(*sharedCameraInfo);
    } else {
        ROS_WARN("Failed to get camera info");
    }
    pcl::PointCloud<point_t>::Ptr point_cloud = depthImageToPointcloud(depth_image, &rgb_image, cam_model);
    try {
        pcl::io::savePCDFile ("point_cloud.pcd", *point_cloud, true);
    } catch(pcl::IOException &e) {
        ROS_WARN("%s", e.what());
    }
    /*if(strcmp(argv[1], "color")==0) {
        if(argc<9) {
            printf("Not enough arguments (color filename hmin hmax smin smax vmin vmax)");
            exit(EXIT_SUCCESS);
        }
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::io::loadPCDFile (argv[2], *cloud);
        cloud->width = (cloud->width) * (cloud->height);
        cloud->height = 1;
        for (size_t j = cloud->points.size(); j--; ) {//This directly extracts the points, but is pretty slow
            float h, s, v;
            rgb2hsv(cloud->points[j].r/255.0, cloud->points[j].g/255.0, cloud->points[j].b/255.0, &h, &s, &v);
            if(h<atof(argv[3]) || h>atof(argv[4]) || s<atof(argv[5]) || s>atof(argv[6]) || v<atof(argv[7]) || v>atof(argv[8]))
                cloud->points.erase(cloud->points.begin() + j);
        }
        cloud->width = cloud->points.size();
        try {
            pcl::io::savePCDFile ("out.pcd", *cloud, true);
        } catch (pcl::IOException &e) {
            ROS_WARN("%s", e.what());
        }
    } else if(strcmp(argv[1], "cluster")==0) {
        if(argc<4) {
            printf("Not enough arguments (cluster filename clustertolerance)");
            exit(EXIT_SUCCESS);
        }
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::io::loadPCDFile (argv[2], *cloud);
        pcl::search::KdTree<point_t>::Ptr tree(new pcl::search::KdTree<point_t>);
        tree->setInputCloud(cloud);

        std::vector<pcl::PointIndices> cluster_indices;
        pcl::EuclideanClusterExtraction<point_t> ec;
        ec.setClusterTolerance(atof(argv[3])); // e.g. 1cm
        ec.setMinClusterSize(500);
        ec.setMaxClusterSize(50000);
        ec.setSearchMethod(tree);
        ec.setInputCloud(cloud);
        ec.extract(cluster_indices);

        printf("segmentation(): cluster extraction done");

        size_t number=0;
        for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); ++it) {
            pcl::PointCloud<point_t>::Ptr cloud_cluster(new pcl::PointCloud<point_t>);
            for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
                cloud_cluster->points.push_back(cloud->points[*pit]);
            cloud_cluster->width = cloud_cluster->points.size();
            cloud_cluster->height = 1;
            cloud_cluster->is_dense = true;

            std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size() << " data points." << std::endl;
            std::ostringstream stream;
            stream << "cluster" << number++ << ".pcd";
            try {
              pcl::io::savePCDFile (stream.str(), *cloud_cluster, true);
            } catch (pcl::IOException &e) {
              ROS_WARN("%s", e.what());
            }
        }
    } else if(strcmp(argv[1], "rotate")==0) {
        if(argc<3) {
            printf("Not enough arguments (rotate filename)");
            exit(EXIT_SUCCESS);
        }
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::io::loadPCDFile (argv[2], *cloud);
        pcl::visualization::PCLVisualizer::Ptr viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
        viewer->setBackgroundColor (0, 0, 0);
        viewer->addPointCloud<pcl::PointXYZRGBA> (cloud, "sample cloud");
        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "sample cloud");
        viewer->initCameraParameters ();
        viewer->registerKeyboardCallback (keyboardEventOccurred, (void*)viewer.get ());
        viewer->spin();
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr result(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::transformPointCloud (*cloud, *result, viewer->getViewerPose().inverse());
        try {
          pcl::io::savePCDFile ("out.pcd", *result, true);
        } catch (pcl::IOException &e) {
          ROS_WARN("%s", e.what());
        }
    } else {
        printf("Unknown command");
    }*/
    // Load data
    /*std::vector<PCD, Eigen::aligned_allocator<PCD> > data;
    loadData (argc, argv, data);

    // Check user input
    if (data.empty ())
    {
        PCL_ERROR ("Syntax is: %s <source.pcd> <target.pcd> [*]", argv[0]);
        PCL_ERROR ("[*] - multiple files can be added. The registration results of (i, i+1) will be registered against (i+2), etc");
        return (-1);
    }
    PCL_INFO ("Loaded %d datasets.", (int)data.size ());
  
    // Create a PCLVisualizer object
    p = new pcl::visualization::PCLVisualizer (argc, argv, "Pairwise Incremental Registration example");
    p->createViewPort (0.0, 0, 0.5, 1.0, vp_1);
    p->createViewPort (0.5, 0, 1.0, 1.0, vp_2);

    PointCloud::Ptr result (new PointCloud), source, target;
    Eigen::Matrix4f GlobalTransform = Eigen::Matrix4f::Identity (), pairTransform;
  
    for (size_t i = 1; i < data.size (); ++i) {
        source = data[i-1].cloud;
        target = data[i].cloud;

        // Add visualization data
        showCloudsLeft(source, target);

        PointCloud::Ptr temp (new PointCloud);
        PCL_INFO ("Aligning %s (%d) with %s (%d).\n", data[i-1].f_name.c_str (), source->points.size (), data[i].f_name.c_str (), target->points.size ());
        pairAlign (source, target, temp, pairTransform, true);

        //transform current pair into the global transform
        pcl::transformPointCloud (*temp, *result, GlobalTransform);

        //update the global transform
        GlobalTransform *= pairTransform;

        //save aligned pair, transformed into the first cloud's frame
        std::stringstream ss;
        ss << i << ".pcd";
        try {
          pcl::io::savePCDFile (ss.str (), *result, true);
        } catch (pcl::IOException &e) {
          ROS_WARN("%s", e.what());
        }
    }*/
}
