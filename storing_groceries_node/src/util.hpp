/**
 * @file
 * @author  Markus Vieth
 * @section DESCRIPTION
 *
 * Util stuff
 */
#pragma once
#include <Eigen/Geometry>
//#include <Eigen/src/Geometry/Quaternion.h>  // for Quaternionf
//#include <Eigen/src/Geometry/Transform.h>   // for Affine3d, Isometry3d, Tra...
#include <tf2_ros/buffer.h> // for tf2_ros::Buffer
// msgs
#include <sensor_msgs/Image.h>
#include <shape_msgs/Plane.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Vector3.h>

std::ostream &operator<<(std::ostream &os, const Eigen::Quaternionf &q);

// Source: https://stackoverflow.com/a/10758845
template <typename T>
std::ostream& operator<< (std::ostream& out, const std::vector<T>& v) {
  if ( !v.empty() ) {
    out << '[';
    std::copy (v.begin(), v.end(), std::ostream_iterator<T>(out, ", "));
    out << "\b\b]";
  }
  return out;
}

// Source: https://stackoverflow.com/questions/3018313/algorithm-to-convert-rgb-to-hsv-and-hsv-to-rgb-in-range-0-255-for-both
// Note that the inputs and outputs, except for the angle in degrees (hue), are in the range of 0 to 1.0. This code does no real sanity checking on inputs. Proceed with caution!
void hsv2rgb(double h, double s, double v, float &r, float &g, float &b);
void rgb2hsv(double r, double g, double b, float &h, float &s, float &v);

/**
 * Like in tf2_eigen, but with Vector3f instead of Vector3d/Quaternionf instead of Quaterniond
 */
namespace tf2 {
  inline
  geometry_msgs::Point toMsg(const Eigen::Vector3f& in)
  {
    geometry_msgs::Point msg;
    msg.x = in.x();
    msg.y = in.y();
    msg.z = in.z();
    return msg;
  }
 
  inline
  void fromMsg(const geometry_msgs::Point& msg, Eigen::Vector3f& out)
  {
    out.x() = msg.x;
    out.y() = msg.y;
    out.z() = msg.z;
  }
 
  inline
  geometry_msgs::Vector3& toMsg(const Eigen::Vector3f& in, geometry_msgs::Vector3& out)
  {
    out.x = in.x();
    out.y = in.y();
    out.z = in.z();
    return out;
  }
 
  inline
  void fromMsg(const geometry_msgs::Vector3& msg, Eigen::Vector3f& out)
  {
    out.x() = msg.x;
    out.y() = msg.y;
    out.z() = msg.z;
  }

  inline
  void fromMsg(const geometry_msgs::Quaternion& msg, Eigen::Quaternionf& out) {
    out = Eigen::Quaternionf(msg.w, msg.x, msg.y, msg.z);
  }
  inline
    geometry_msgs::Quaternion toMsg(const Eigen::Quaternionf& in) {
    geometry_msgs::Quaternion msg;
    msg.w = in.w();
    msg.x = in.x();
    msg.y = in.y();
    msg.z = in.z();
    return msg;
  }
}

/**
 * Search the transformation from source_frame to target_frame in the tfBuffer, and return it as an affine transform.
 */
/*Eigen::Affine3d getAffineTransform(const tf2_ros::Buffer &tfBuffer,
                                   const std::string &target_frame,
                                   const std::string &source_frame);*/

/**
 * Search the transformation from source_frame to target_frame in the tfBuffer, and return it as an isometry transform.
 */
Eigen::Isometry3d getIsometryTransform(const tf2_ros::Buffer &tfBuffer,
                                   const std::string &target_frame,
                                   const std::string &source_frame);

Eigen::Isometry3d affine2isometry(const Eigen::Affine3d& t);
Eigen::Affine3d isometry2affine(const Eigen::Isometry3d& t);

/**
 * xmax and ymax are exclusive, i.e. the last row/column of the sub-image is row ymax-1 / column xmax-1
 */
sensor_msgs::Image getSubImage(const sensor_msgs::Image &input,
                               uint32_t xmin, uint32_t ymin, uint32_t xmax, uint32_t ymax);

/**
 * Intersection point (xs,ys) of two lines a1*x+b1*y=c1 and a2*x+b2*y=c2.
 * Returns false if lines are parallel and no solution can be found
 * symmetric
 */
bool intersectionPointOfTwoLines2D(double a1, double b1, double c1,
                                   double a2, double b2, double c2,
                                   double &xs, double &ys);

/**
 * Intersection point (ix,iy,iz) of a plane (ax + by + cz + d = 0) and a line ((x,y,z)=r*(lx,ly,lz) with r in real).
 * @return true if intersection point has been found, false if line and plane are parallel (no intersection point or infinitely many)
 */
bool intersectionPointOfLineAndPlane(double a, double b, double c, double d,
                                     double lx, double ly, double lz,
                                     double& ix, double& iy, double& iz);

/**
 * Distance of point (x1, y1, z1) to plane a*x+b*y+c*z=d, but signed. To get the "real" distance, TODO use other plane equation ax+by+cz+d=0?
 * just use "fabs". Can also be used for 2D (distance of point to line), if c=0. Return value is 0 if length of normal vector (a, b, c) is too small.
 */
double signedDistancePointToPlane(double a, double b, double c, double d,
                                  double x1, double y1, double z1);

/**
 * Distance of the point (x0, y0) to the line defined by points (x1, y1) and (x2, y2)
 * Return value is always greater or equal to 0. Return value is INFINITY if (x1, y1)==(x2, y2)
 */
double distPointToLine2D(double x0, double y0, double x1, double y1, double x2, double y2);

/**
 * Distance of the point x0 to the line defined by points x1 and x2 in 3D.
 * Return value is always greater or equal to 0. Return value is INFINITY if x1==x2
 * TODO not really tested
 */
double distPointToLine3D(const Eigen::Vector3f& x0, const Eigen::Vector3f& x1, const Eigen::Vector3f& x2);

/**
 * Calculate the angle between the two vectors (x1, y1, z1) and (x2, y2, z2), in the range of [0; pi]. Symmetric.
 */
double angleBetweenVectors(double x1, double y1, double z1,
                           double x2, double y2, double z2);

// Based on https://stackoverflow.com/questions/5782658/extracting-yaw-from-a-quaternion
double yaw_from_quat(const geometry_msgs::Quaternion & quat); // TODO does this really work?

/**
 * Keeps the angle in the intervall (-pi; pi]
 */
double angleModulo(double angle);

/** Generate a random (hopefully uniform) quaternion. Source: http://planning.cs.uiuc.edu/node198.html
 */
void randomQuaternion(double& x, double& y, double& z, double& w);

/**
 * Transform the plane ax + by + cz + d = 0, (a,b,c) is normal vector using the transform e
 * Better use the other method with Isometry3d.
 * Source: https://stackoverflow.com/questions/7685495/transforming-a-3d-plane-using-a-4x4-matrix
 */
shape_msgs::Plane transformPlane(const double a, const double b, const double c, const double d,
                                 const Eigen::Affine3d& e);

/**
 * Transform the plane ax + by + cz + d = 0, (a,b,c) is normal vector using the transform e
 * Source: https://stackoverflow.com/questions/7685495/transforming-a-3d-plane-using-a-4x4-matrix
 */
shape_msgs::Plane transformPlane(const double a, const double b, const double c, const double d,
                                 const Eigen::Isometry3d& e);
