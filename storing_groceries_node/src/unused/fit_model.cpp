#include <Eigen/Core>
#include <eigen_conversions/eigen_msg.h>
#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/common/time.h>
#include <pcl/console/print.h>
#include <pcl/features/fpfh_omp.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/point_types.h>
#include <pcl/registration/gicp.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/sample_consensus_prerejective.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>

typedef pcl::PointNormal PointNT;
typedef pcl::PointCloud<PointNT> PointCloudT;
typedef pcl::FPFHSignature33 FeatureT;
typedef pcl::FPFHEstimationOMP<PointNT, PointNT, FeatureT> FeatureEstimationT;
typedef pcl::PointCloud<FeatureT> FeatureCloudT;
typedef pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ>
    ColorHandlerT;
typedef pcl::visualization::PointCloudColorHandlerCustom<PointNT>
    ColorHandler2T;

void fitModel3(pcl::PointCloud<pcl::PointXYZ>::Ptr scene_input, float box_width,
               float box_height) {
  // Point clouds
  PointCloudT::Ptr object(new PointCloudT);
  PointCloudT::Ptr object_aligned(new PointCloudT);
  PointCloudT::Ptr scene(new PointCloudT);
  FeatureCloudT::Ptr object_features(new FeatureCloudT);
  FeatureCloudT::Ptr scene_features(new FeatureCloudT);

  // Get input object and scene
  // if (argc != 3)
  //{
  //  pcl::console::print_error ("Syntax is: %s object.pcd scene.pcd\n",
  //  argv[0]);
  //  return (1);
  //}

  // Load object and scene
  // pcl::console::print_highlight ("Loading point clouds...\n");
  // if (pcl::io::loadPCDFile<PointNT> (argv[1], *object) < 0 ||
  //    pcl::io::loadPCDFile<PointNT> (argv[2], *scene) < 0)
  //{
  //  pcl::console::print_error ("Error loading object/scene file!\n");
  //  return (1);
  //}
  scene->width = scene_input->width;
  scene->height = scene_input->height;
  scene->points.resize(scene_input->points.size());
  for (size_t i = 0; i < scene_input->points.size(); i++) {
    scene->points[i].x = scene_input->points[i].x;
    scene->points[i].y = scene_input->points[i].y;
    scene->points[i].z = scene_input->points[i].z;
  }
  ROS_INFO("Generating points");
  object->width = 3 * 3240;
  object->height = 1;
  object->points.resize(object->width * object->height);
  size_t k = 0;
  for (int l = 1; l < 4; l++) {
    for (float i = -box_width / 2.0; i < box_width / 2.0; i += 0.01f) {
      for (float j = -box_height / 2.0; j < box_height / 2.0; j += 0.01f) {
        object->points[k].x = j + 1.0;
        object->points[k].y = i;
        object->points[k].z = l * 0.4;
        k++;
      }
    }
  }
  ROS_INFO("k=%lu\n", k);
  ROS_INFO("Generated box cloud");

  // Downsample
  pcl::console::print_highlight("Downsampling...\n");
  pcl::VoxelGrid<PointNT> grid;
  const float leaf = 0.005f;
  grid.setLeafSize(leaf, leaf, leaf);
  grid.setInputCloud(object);
  grid.filter(*object);
  grid.setInputCloud(scene);
  grid.filter(*scene);

  // Estimate normals for scene
  pcl::console::print_highlight("Estimating scene normals...\n");
  pcl::NormalEstimationOMP<PointNT, PointNT> nest;
  nest.setRadiusSearch(0.01);
  nest.setInputCloud(scene);
  nest.compute(*scene);

  // Estimate features
  pcl::console::print_highlight("Estimating features...\n");
  ROS_INFO_STREAM("object cloud size:" << object->points.size() << " ");
  FeatureEstimationT fest;
  fest.setRadiusSearch(0.025);
  fest.setInputCloud(object);
  fest.setInputNormals(object);
  fest.compute(*object_features);
  fest.setInputCloud(scene);
  fest.setInputNormals(scene);
  fest.compute(*scene_features);

  // Perform alignment
  pcl::console::print_highlight("Starting alignment...\n");
  pcl::SampleConsensusPrerejective<PointNT, PointNT, FeatureT> align;
  align.setInputSource(object);
  align.setSourceFeatures(object_features);
  align.setInputTarget(scene);
  align.setTargetFeatures(scene_features);
  align.setMaximumIterations(50000); // Number of RANSAC iterations
  align.setNumberOfSamples(
      3); // Number of points to sample for generating/prerejecting a pose
  align.setCorrespondenceRandomness(5); // Number of nearest features to use
  align.setSimilarityThreshold(
      0.9f); // Polygonal edge length similarity threshold
  align.setMaxCorrespondenceDistance(2.5f * leaf); // Inlier threshold
  align.setInlierFraction(
      0.25f); // Required inlier fraction for accepting a pose hypothesis
  {
    pcl::ScopeTime t("Alignment");
    align.align(*object_aligned);
  }

  if (align.hasConverged()) {
    // Print results
    printf("\n");
    Eigen::Matrix4f transformation = align.getFinalTransformation();
    pcl::console::print_info("    | %6.3f %6.3f %6.3f | \n",
                             transformation(0, 0), transformation(0, 1),
                             transformation(0, 2));
    pcl::console::print_info("R = | %6.3f %6.3f %6.3f | \n",
                             transformation(1, 0), transformation(1, 1),
                             transformation(1, 2));
    pcl::console::print_info("    | %6.3f %6.3f %6.3f | \n",
                             transformation(2, 0), transformation(2, 1),
                             transformation(2, 2));
    pcl::console::print_info("\n");
    pcl::console::print_info("t = < %0.3f, %0.3f, %0.3f >\n",
                             transformation(0, 3), transformation(1, 3),
                             transformation(2, 3));
    pcl::console::print_info("\n");
    pcl::console::print_info("Inliers: %i/%i\n", align.getInliers().size(),
                             object->size());

    // Show alignment
    pcl::visualization::PCLVisualizer visu("Alignment");
    visu.addPointCloud(scene, ColorHandler2T(scene, 0.0, 255.0, 0.0), "scene");
    visu.addPointCloud(object_aligned,
                       ColorHandler2T(object_aligned, 0.0, 0.0, 255.0),
                       "object_aligned");
    visu.spin();
  } else {
    pcl::console::print_error("Alignment failed!\n");
    return;
  }

  return;
}

void fitModel2(pcl::PointCloud<pcl::PointXYZ>::Ptr scene_input, float box_width,
               float box_height) {
  pcl::PointCloud<PointNT>::Ptr object(new pcl::PointCloud<PointNT>);
  PointCloudT::Ptr object_aligned(new PointCloudT);
  PointCloudT::Ptr scene(new PointCloudT);
  scene->points.resize(scene_input->size());
  for (size_t i = 0; i < scene_input->points.size(); i++) {
    scene->points[i].x = scene_input->points[i].x;
    scene->points[i].y = scene_input->points[i].y;
    scene->points[i].z = scene_input->points[i].z;
  }
  FeatureCloudT::Ptr object_features(new FeatureCloudT);
  FeatureCloudT::Ptr scene_features(new FeatureCloudT);
  object->width = 3240; //(box_width/0.01+1)*(box_height/0.01+1);
  object->height = 1;
  object->points.resize(object->width * object->height);
  ROS_INFO("cloud size:%lu\n", object->points.size());

  ROS_INFO("Generating points");

  size_t k = 0;
  for (float i = -box_width / 2.0; i < box_width / 2.0; i += 0.01f) {
    for (float j = -box_height / 2.0; j < box_height / 2.0; j += 0.01f) {
      object->points[k].x = j + 1.0;
      object->points[k].y = i;
      object->points[k].z = 0.0;
      k++;
    }
  }
  ROS_INFO("k=%lu\n", k);

  ROS_INFO("Generated box cloud");

  // Downsample
  pcl::console::print_highlight("Downsampling...\n");
  pcl::VoxelGrid<PointNT> grid;
  const float leaf = 0.005f;
  grid.setLeafSize(leaf, leaf, leaf);
  grid.setInputCloud(object);
  grid.filter(*object);
  grid.setInputCloud(scene);
  grid.filter(*scene);

  /*pcl::PointCloud<PointNT>::Ptr cloud_projected (new
  pcl::PointCloud<PointNT>);
  pcl::PointCloud<PointNT>::Ptr cloud_filtered (new pcl::PointCloud<PointNT>);
  pcl::PointCloud<PointNT>::Ptr cloud (new pcl::PointCloud<PointNT>);

  pcl::PassThrough<PointNT> pass;
  pass.setInputCloud (scene);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (0.1, 4.0);
  //pass.setFilterLimitsNegative (true);
  pass.filter (*cloud);

  // Create a set of planar coefficients with X=Y=0,Z=1
  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
  coefficients->values.resize (4);
  coefficients->values[0] = coefficients->values[1] = 0;
  coefficients->values[2] = 1.0;
  coefficients->values[3] = 0;

  // Create the filtering object
  pcl::ProjectInliers<PointNT> proj;
  proj.setModelType (pcl::SACMODEL_PLANE);
  proj.setInputCloud (cloud);
  proj.setModelCoefficients (coefficients);
  proj.filter (*scene);

  ROS_INFO("Filtered cloud");*/

  // Estimate normals for scene
  pcl::console::print_highlight("Estimating scene normals...\n");
  pcl::NormalEstimationOMP<PointNT, PointNT> nest;
  nest.setRadiusSearch(0.01);
  nest.setInputCloud(scene);
  nest.compute(*scene);
  nest.setInputCloud(object);
  nest.compute(*object);

  // Estimate features
  pcl::console::print_highlight("Estimating features...\n");
  FeatureEstimationT fest;
  fest.setRadiusSearch(0.025);

  ROS_INFO("Estimating scene");
  fest.setInputCloud(scene);
  fest.setInputNormals(scene);
  fest.compute(*scene_features);
  ROS_INFO("Estimating object");
  fest.setInputCloud(object);
  fest.setInputNormals(object);
  fest.compute(*object_features);

  // Perform alignment
  pcl::console::print_highlight("Starting alignment...\n");
  pcl::SampleConsensusPrerejective<PointNT, PointNT, FeatureT> align;
  align.setInputSource(object);
  align.setSourceFeatures(object_features);
  align.setInputTarget(scene);
  align.setTargetFeatures(scene_features);
  align.setMaximumIterations(50000); // Number of RANSAC iterations
  align.setNumberOfSamples(
      3); // Number of points to sample for generating/prerejecting a pose
  align.setCorrespondenceRandomness(5); // Number of nearest features to use
  align.setSimilarityThreshold(
      0.9f); // Polygonal edge length similarity threshold
  align.setMaxCorrespondenceDistance(2.5f * leaf); // Inlier threshold
  align.setInlierFraction(
      0.25f); // Required inlier fraction for accepting a pose hypothesis
  {
    pcl::ScopeTime t("Alignment");
    align.align(*object_aligned);
  }

  if (align.hasConverged()) {
    // Print results
    printf("\n");
    Eigen::Matrix4f transformation = align.getFinalTransformation();
    pcl::console::print_info("    | %6.3f %6.3f %6.3f | \n",
                             transformation(0, 0), transformation(0, 1),
                             transformation(0, 2));
    pcl::console::print_info("R = | %6.3f %6.3f %6.3f | \n",
                             transformation(1, 0), transformation(1, 1),
                             transformation(1, 2));
    pcl::console::print_info("    | %6.3f %6.3f %6.3f | \n",
                             transformation(2, 0), transformation(2, 1),
                             transformation(2, 2));
    pcl::console::print_info("\n");
    pcl::console::print_info("t = < %0.3f, %0.3f, %0.3f >\n",
                             transformation(0, 3), transformation(1, 3),
                             transformation(2, 3));
    pcl::console::print_info("\n");
    pcl::console::print_info("Inliers: %i/%i\n", align.getInliers().size(),
                             object->size());

    // Show alignment
    pcl::visualization::PCLVisualizer visu("Alignment");
    visu.addPointCloud(scene, ColorHandler2T(scene, 0.0, 255.0, 0.0), "scene");
    visu.addPointCloud(object_aligned,
                       ColorHandler2T(object_aligned, 0.0, 0.0, 255.0),
                       "object_aligned");
    visu.spin();
  } else {
    pcl::console::print_error("Alignment failed!\n");
    return;
  }
}

void fitModel(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_input, float box_width,
              float box_height) {
  ROS_INFO("Start fitModel");
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_projected(
      new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(
      new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);

  pcl::PassThrough<pcl::PointXYZ> pass;
  pass.setInputCloud(cloud_input);
  pass.setFilterFieldName("z");
  pass.setFilterLimits(0.1, 4.0);
  // pass.setFilterLimitsNegative (true);
  pass.filter(*cloud);

  // Create a set of planar coefficients with X=Y=0,Z=1
  pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients());
  coefficients->values.resize(4);
  coefficients->values[0] = coefficients->values[1] = 0;
  coefficients->values[2] = 1.0;
  coefficients->values[3] = 0;

  // Create the filtering object
  pcl::ProjectInliers<pcl::PointXYZ> proj;
  proj.setModelType(pcl::SACMODEL_PLANE);
  proj.setInputCloud(cloud);
  proj.setModelCoefficients(coefficients);
  proj.filter(*cloud_projected);

  ROS_INFO("Filtered cloud");

  // Voxel
  pcl::VoxelGrid<pcl::PointXYZ> sor;
  sor.setInputCloud(cloud_projected);
  sor.setLeafSize(0.01f, 0.01f, 0.01f);
  sor.filter(*cloud_filtered);

  ROS_INFO("Making box_cloud");

  pcl::PointCloud<pcl::PointXYZ>::Ptr box_cloud(
      new pcl::PointCloud<pcl::PointXYZ>);
  box_cloud->width = (box_width / 0.01 + 1) * (box_height / 0.01 + 1);
  box_cloud->height = 1;
  box_cloud->points.resize(box_cloud->width * box_cloud->height);
  ROS_INFO("cloud size:%lu\n", box_cloud->points.size());

  ROS_INFO("Generating points");

  size_t k = 0;
  for (float i = -box_width / 2.0; i < box_width / 2.0; i += 0.01f) {
    for (float j = -box_height / 2.0; j < box_height / 2.0; j += 0.01f) {
      box_cloud->points[k].x = j + 1.0;
      box_cloud->points[k].y = i;
      box_cloud->points[k].z = 0.0;
      k++;
    }
  }
  ROS_INFO("k=%lu\n", k);

  ROS_INFO("Generated box cloud");

  pcl::GeneralizedIterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
  icp.setInputSource(box_cloud);
  icp.setInputTarget(cloud_filtered);
  pcl::PointCloud<pcl::PointXYZ>::Ptr Final(new pcl::PointCloud<pcl::PointXYZ>);
  ROS_INFO("starting align");
  icp.align(*Final);
  std::cout << "has converged:" << icp.hasConverged()
            << " score: " << icp.getFitnessScore() << std::endl;
  std::cout << icp.getFinalTransformation() << std::endl;

  pcl::visualization::PCLVisualizer visu("Alignment");
  visu.addPointCloud(cloud, ColorHandlerT(cloud, 0.0, 255.0, 0.0), "cloud");
  visu.addPointCloud(cloud_filtered,
                     ColorHandlerT(cloud_filtered, 255.0, 0.0, 0.0),
                     "footprint");
  visu.addPointCloud(box_cloud, ColorHandlerT(box_cloud, 0.0, 0.0, 255.0),
                     "box");
  visu.addPointCloud(Final, ColorHandlerT(Final, 255.0, 0.0, 255.0), "final");
  visu.spin();
}

/*int main(int argc, char** argv) {
    ros::init(argc, argv, "storing_groceries_server");
    ros::NodeHandle nh;
    tf2_ros::Buffer tfBuffer;
tf2_ros::TransformListener tfListener(tfBuffer);
    ros::WallDuration(0.5).sleep();
    //Get PointCloud
    //Adapted from
https://answers.ros.org/question/293890/how-to-use-waitformessage-properly/
    boost::shared_ptr<sensor_msgs::PointCloud2 const> sharedCloud =
ros::topic::waitForMessage<sensor_msgs::PointCloud2>("/xtion/depth_registered/points",
nh);
    sensor_msgs::PointCloud2 msg_in;
    if(sharedCloud != NULL){
        msg_in = *sharedCloud;
    }
    ros::WallDuration(0.5).sleep();

    //Transform the right frame
    geometry_msgs::TransformStamped transformStamped;
    try{
      transformStamped = tfBuffer.lookupTransform("base_footprint",
msg_in.header.frame_id, msg_in.header.stamp);
    }
    catch (tf2::TransformException &ex) {
      ROS_WARN("%s",ex.what());
      return -1;
    }
    Eigen::Affine3d e;
    tf::transformMsgToEigen(transformStamped.transform, e);
    sensor_msgs::PointCloud2 transCloud;
    pcl_ros::transformPointCloud(e.matrix().cast<float>(), msg_in, transCloud);

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new
pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg(transCloud, *cloud);

    fitModel3(cloud, 0.8, 0.4);
    //cluster(cloud);
}*/
