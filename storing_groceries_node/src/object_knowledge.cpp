#include "object_knowledge.hpp"
#include <ros/node_handle.h>
#include <yaml-cpp/yaml.h>
#include <ctype.h>                      // for isalpha
#include <math.h>                       // for fabsf
#include <stddef.h>                     // for ptrdiff_t
#include <stdio.h>                      // for printf, puts
#include <string.h>                     // for size_t, strcmp, NULL
#include <algorithm>                    // for sort, replace, find, remove_if
#include <functional>                   // for greater
#include <iostream>                     // for operator<<, basic_ostream
#include <memory>                       // for allocator_traits<>::value_type
#include <utility>                      // for pair
#include <ros/console.h>                // for LogLocation, ROS_ERROR, ROS_ERROR_STREAM

// Filled in by yaml config file
std::vector<std::string> categories;
std::vector<Object> possibleObjects;

std::map<int, std::string> objectLabels;

std::ostream &operator<<(std::ostream &os, const Object &obj) {
  os << "name:" << obj.name << " category:" << (obj.category < categories.size()
                                                    ? categories[obj.category]
                                                    : "wrongcategory")
     << " primitive:";
  switch (obj.primitive) {
  case OTHER:
    os << "other";
    break;
  case BOX:
    os << "box";
    break;
  case SPHERE:
    os << "sphere";
    break;
  case CYLINDER:
    os << "cylinder";
    break;
  case CONE:
    os << "cone";
    break;
  default:
    os << "wrongprimitive";
    ROS_ERROR("Field primitive of Object contains wrong value");
  }
  os << " dim0:" << obj.dim0 << " dim1:" << obj.dim1 << " dim2:" << obj.dim2;
  return os;
}

namespace YAML {
template <> struct convert<Object> {
  static Node encode(const Object &rhs) {
    Node node;
    // TODO untested
    node["name"] = rhs.name;
    node["category"] = rhs.category < categories.size()
                           ? categories[rhs.category]
                           : "wrongcategory";
    switch (rhs.primitive) {
    case OTHER:
      node["primitive"] = "other";
      break;
    case BOX:
      node["primitive"] = "box";
      break;
    case SPHERE:
      node["primitive"] = "sphere";
      break;
    case CYLINDER:
      node["primitive"] = "cylinder";
      break;
    case CONE:
      node["primitive"] = "cone";
      break;
    default:
      node["primitive"] = "wrongprimitive";
      ROS_ERROR("Field primitive of Object contains wrong value");
    }
    node["dim0"] = rhs.dim0;
    node["dim1"] = rhs.dim1;
    node["dim2"] = rhs.dim2;
    return node;
  }

  static bool decode(const Node &node, Object &rhs) {
    // if(!node.IsSequence() || node.size() != 3) {
    //  return false;
    //}
    rhs.name = node["name"].as<std::string>();
    std::string category_string = node["category"].as<std::string>();
    ptrdiff_t pos =
        find(categories.begin(), categories.end(), category_string) -
        categories.begin();
    rhs.category = (pos < 0 || pos > 255 || ((size_t)pos) >= categories.size())
                       ? 255
                       : pos;
    std::string primitive_string = node["primitive"].as<std::string>();
    if (primitive_string == "other") {
      rhs.primitive = OTHER;
    } else if (primitive_string == "box") {
      rhs.primitive = BOX;
    } else if (primitive_string == "sphere") {
      rhs.primitive = SPHERE;
    } else if (primitive_string == "cylinder") {
      rhs.primitive = CYLINDER;
    } else if (primitive_string == "cone") {
      rhs.primitive = CONE;
    } else {
      ROS_ERROR_STREAM("Unknown primitive: " << primitive_string);
    }
    float dims[3] = {node["dim0"].as<float>(), node["dim1"].as<float>(),
                     node["dim2"].as<float>()};
    if (rhs.primitive == OTHER || rhs.primitive == BOX)
      std::sort(dims, dims + 3, std::greater<float>());
    rhs.dim0 = dims[0];
    rhs.dim1 = dims[1];
    rhs.dim2 = dims[2];
    if (rhs.primitive == SPHERE &&
        (rhs.dim0 != rhs.dim2 || rhs.dim1 != rhs.dim2))
      ROS_WARN("While decoding yaml file: sphere object has wrong dimensions: "
               "%f %f %f",
               rhs.dim0, rhs.dim1, rhs.dim2);
    if ((rhs.primitive == CYLINDER || rhs.primitive == CONE) &&
        rhs.dim1 != rhs.dim2)
      ROS_WARN("While decoding yaml file: cylinder or cone object has wrong "
               "dimensions: %f %f %f",
               rhs.dim0, rhs.dim1, rhs.dim2);
    return true;
  }
};
}

bool parseConfigYAML(const std::string yaml_filename) {
  if(yaml_filename.empty()) {
    ROS_ERROR("parseConfigYAML(): filename is empty");
    return false;
  }
  try {
    YAML::Node node = YAML::LoadFile(yaml_filename);
    categories.clear();
    possibleObjects.clear();
    categories = node["categories"].as<std::vector<std::string>>();
    possibleObjects = node["objects"].as<std::vector<Object>>();
  } catch (YAML::BadFile &e) {
    ROS_ERROR_STREAM("Bad filename or file: >" << yaml_filename << "<, could not read config file.\n");
    return false;
  }
  for (size_t i = 0; i < categories.size(); i++) {
    puts(categories[i].c_str());
  }
  for (size_t i = 0; i < possibleObjects.size(); i++) {
    std::cout << possibleObjects[i] << std::endl;
  }
  return true;
}

std::vector<Object>& getPossibleObjects() {
    return possibleObjects;
}

std::vector<std::string>& getCategories() {
    return categories;
}

int classifyObject(float x, float y, float z) {
  float dims[3] = {x, y, z};
  std::sort(dims, dims + 3);
  if (dims[2] < 0.3) {
    return 0;
  }
  return 1;
}

std::multimap<float, std::string> classifyBySize(float x, float y, float z) {
  printf("classifyBySize(%f, %f, %f)\n", x, y, z);
  float dims[3] = {x, y, z};
  std::sort(dims, dims + 3);
  std::multimap<float, std::string> result;
  float scoresum = 0.0;
  std::vector<float> scores(possibleObjects.size());
  for (size_t i = 0; i < possibleObjects.size(); i++) {
    float dims2[3] = {possibleObjects[i].dim0, possibleObjects[i].dim1,
                      possibleObjects[i].dim2};
    std::sort(dims2, dims2 + 3);
    float score =
        1.0 / (fabsf(dims[0] - dims2[0]) + fabsf(dims[1] - dims2[1]) +
               fabsf(dims[2] - dims2[2])); // Alternative: difference of volume,
                                           // but that is unsuited, because a
                                           // 2x2x2 object and a 1x2x4 object
                                           // have the same volume
    scores[i] = score;
    scoresum += score;
  }
  for (size_t i = 0; i < possibleObjects.size(); i++) {
    result.insert(std::pair<float, std::string>(scores[i] / scoresum,
                                                possibleObjects[i].name));
  }
  return result;
}

std::string getObjectLabelById(const ros::NodeHandle &nh,
                               const int id) { // TODO Maybe use getParamCached?
  if (objectLabels.count(id) == 1) {
    return objectLabels[id];
  } else {
    std::ostringstream oss;
    oss << "/object_labels/" << id;
    std::string object_label;
    if (nh.getParam(oss.str(), object_label)) {
      std::replace(object_label.begin(), object_label.end(), ' ', '_');
      objectLabels[id] = object_label;
      return object_label;
    } else {
      return std::to_string(id);
    }
  }
}

int getIdByObjectLabel(const ros::NodeHandle &nh,
                       const std::string label) { // TODO Maybe use getParamCached?
  ROS_DEBUG_STREAM("getIdByObjectLabel called, searching id for label " << label);
  // First go through the map objectLabels to see if it is cached there
  for(auto it=objectLabels.begin(); it!=objectLabels.end(); it++) {
    ROS_DEBUG_STREAM("Comparing with: " << it->first << " " << it->second);
    if(it->second == label) {
        ROS_DEBUG_STREAM("Fits! Returning id " << it->first);
        return it->first;
    }
  }
  // Not found
  ROS_DEBUG("Not found, searching on parameter server");
  for(size_t i=0; ; i++) {
    std::ostringstream oss;
    oss << "/object_labels/" << i;
    std::string object_label;
    if (nh.getParam(oss.str(), object_label)) {
      std::replace(object_label.begin(), object_label.end(), ' ', '_');
      objectLabels[i] = object_label;
      if (object_label == label) {
        ROS_DEBUG_STREAM("Returning id " << i);
        return i;
      }
    } else {
      ROS_ERROR_STREAM("getIdByObjectLabel: id not found for label " << label);
      return -1;
    }
  }
}

uint8_t getCategoryOfObject(const std::string name) {
  for (size_t i = 0; i < possibleObjects.size(); i++) {
    if (strcmp(name.c_str(), possibleObjects[i].name.c_str()) == 0) {
      return possibleObjects[i].category;
    }
  }
  ROS_WARN_STREAM("getCategoryOfObject: could not find object: >" << name
                                                                  << "<");
  return 255;
}

primitive_t getPrimitiveOfObject(const std::string name) {
  for (size_t i = 0; i < possibleObjects.size(); i++) {
    if (strcmp(name.c_str(), possibleObjects[i].name.c_str()) == 0) {
      return possibleObjects[i].primitive;
    }
  }
  ROS_WARN_STREAM("getPrimitiveOfObject: could not find object: " << name);
  return OTHER;
}

Object *getObject(const std::string name) {
  for (size_t i = 0; i < possibleObjects.size(); i++) {
    if (strcmp(name.c_str(), possibleObjects[i].name.c_str()) == 0) {
      return &possibleObjects[i];
    }
  }
  ROS_WARN_STREAM("getObject: could not find object: " << name);
  return NULL;
}

bool my_predicate(char c) { return !isalpha(c) && c != '_'; }

std::string getNameFromId(const std::string in) {
  std::string s(in);
  s.erase(std::remove_if(s.begin(), s.end(), my_predicate), s.end());
  return s;
  // for(size_t i=0; i<s.size(); i++) {
  //    if(my_predicate(s[i])) {
  //        s.erase();
  //    }
  //}
}
