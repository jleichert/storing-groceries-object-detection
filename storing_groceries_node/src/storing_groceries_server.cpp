/* Author: Markus Vieth*/
#include <memory>
#include <time.h>

// ros and libraries
#include <Eigen/Geometry>
#include <actionlib/server/simple_action_server.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl_ros/transforms.h>
#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>

// Messages
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <control_msgs/JointTrajectoryControllerState.h>
#include <eigen_conversions/eigen_msg.h>
#include <storing_groceries_msgs/GroceriesAction.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#include "high_level_functions.hpp" // for getObjectsAndAddToPlanningScene
//#include "object_knowledge.hpp"
#include "pick_place.hpp" // for initActionClients
#include "point_clouds.hpp" // for getPointCloud
#include "util.hpp" // for getIsometryTransform

//#define CARRY_POSE 0.07, -0.64, -2.87, 2.29, 0.0, -0.34, -0.21
#define CARRY_POSE 0.07, -1.0, -0.24, 2.06, -1.5, 0.77, 0.15 // Kinda like home, but better
//#define OBJECTS_YAML_PATH
//"/media/vol/robocup/tiago-sim/share/storing_groceries_node/config/testing.yaml"

std::string pointcloud_topic;
std::string rgb_image_topic;
const std::string rgb_camera_info_topic = "/xtion/rgb/camera_info";

// Initialized by main
ros::NodeHandle *nh;
moveit::planning_interface::PlanningSceneInterface *planning_scene_interface;
moveit::planning_interface::MoveGroupInterface *group;
actionlib::SimpleActionServer<storing_groceries_msgs::GroceriesAction>
    *scene_as;
actionlib::SimpleActionServer<storing_groceries_msgs::GroceriesAction> *pick_as;
actionlib::SimpleActionServer<storing_groceries_msgs::GroceriesAction>
    *place_as;
// actionlib::SimpleActionServer<storing_groceries_msgs::GroceriesAction> *
// clean_as;
tf2_ros::Buffer tfBuffer(ros::Duration(15.0)); // Default is 10
tf2_ros::TransformListener *tfListener;

std::vector<vision_msgs::Detection3D> shelfObjects;

bool isObjectInGripper = false;
moveit_msgs::CollisionObject objectInGripper; // Or pointer?

std::string supportSurfaceId; // supportSurfaceId e.g. table, for all object
                              // TODO is there a better solution for this?

double getGripperOpening() {
  // Adapted from
  // https://answers.ros.org/question/293890/how-to-use-waitformessage-properly/
  boost::shared_ptr<control_msgs::JointTrajectoryControllerState const> shared =
      ros::topic::waitForMessage<control_msgs::JointTrajectoryControllerState>(
          "/gripper_controller/state", *nh, ros::Duration(10, 0));
  control_msgs::JointTrajectoryControllerState msg_in;
  if (shared != NULL) {
    msg_in = *shared;
  } else {
    return -1.0;
  }
#ifdef SIMULATION // TODO remove this conditional by looking at joint_names
  return msg_in.actual.positions[0] + msg_in.actual.positions[1];
#else
  return msg_in.actual.positions[0];
#endif
}

void moveHead(double updown, double leftright = 0.0) {
  actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> ac(
      "/head_controller/follow_joint_trajectory", true);
  ac.waitForServer();
  control_msgs::FollowJointTrajectoryGoal goal;
  goal.trajectory.joint_names.push_back("head_1_joint");
  goal.trajectory.joint_names.push_back("head_2_joint");
  goal.trajectory.points.resize(1);
  goal.trajectory.points[0].positions.push_back(leftright);
  goal.trajectory.points[0].positions.push_back(updown);
  goal.trajectory.points[0].time_from_start = ros::Duration(3, 0);
  ac.sendGoal(goal);
  ac.waitForResult();
}

void addArtWalls() {
  // Add artificial walls to limit "workspace" to the area the robot has seen
  std::vector<moveit_msgs::CollisionObject> collision_objects;
  moveit_msgs::CollisionObject art_wall, art_wall2;
  art_wall.header.frame_id = BASE_FRAME;
  art_wall.id = "art_wall1";
  art_wall.primitives.resize(1);
  art_wall.primitives[0].type = art_wall.primitives[0].BOX;
  art_wall.primitives[0].dimensions.resize(3);
  art_wall.primitives[0].dimensions[0] = 2.0;
  art_wall.primitives[0].dimensions[1] = 1.0;
  art_wall.primitives[0].dimensions[2] = 0.05;
  art_wall.primitive_poses.resize(1);
  art_wall.primitive_poses[0].position.x = 1.7;
  art_wall.primitive_poses[0].position.y = 0.9;
  art_wall.primitive_poses[0].position.z = 1.0;
  tf2::Quaternion orientation;
  orientation.setRPY(M_PI / 2.0, 0.0, 0.45);
  art_wall.primitive_poses[0].orientation = tf2::toMsg(orientation);
  collision_objects.push_back(art_wall);
  art_wall2.header.frame_id = BASE_FRAME;
  art_wall2.id = "art_wall2";
  art_wall2.primitives.resize(1);
  art_wall2.primitives[0].type = art_wall2.primitives[0].BOX;
  art_wall2.primitives[0].dimensions.resize(3);
  art_wall2.primitives[0].dimensions[0] = 2.0;
  art_wall2.primitives[0].dimensions[1] = 1.0;
  art_wall2.primitives[0].dimensions[2] = 0.05;
  art_wall2.primitive_poses.resize(1);
  art_wall2.primitive_poses[0].position.x = 1.7;
  art_wall2.primitive_poses[0].position.y = -0.9;
  art_wall2.primitive_poses[0].position.z = 1.0;
  orientation.setRPY(M_PI / 2.0, 0.0, -0.45);
  art_wall2.primitive_poses[0].orientation = tf2::toMsg(orientation);
  collision_objects.push_back(art_wall2);
  if (planning_scene_interface != NULL) {
    planning_scene_interface->applyCollisionObjects(collision_objects);
  } else {
    ROS_WARN("planning_scene_interface is NULL");
  }
}

// TODO are the objects in reach?
std::multimap<float, moveit_msgs::CollisionObject>
chooseObjectsFromPlanningScene() {
  std::multimap<float, moveit_msgs::CollisionObject> result;
  std::map<std::string, moveit_msgs::CollisionObject> objects =
      planning_scene_interface->getObjects();
  for (std::map<std::string, moveit_msgs::CollisionObject>::const_iterator
           iter = objects.begin();
       iter != objects.end(); ++iter) {
    std::cout << "Key: " << iter->first << std::endl << "Values:" << std::endl;
    std::cout << iter->second << std::endl;
    if ((iter->second.primitives[0].type == iter->second.primitives[0].CYLINDER &&
         2.0 * iter->second.primitives[0].dimensions[1] < MAX_GRIPPER_OPEN) ||
        (iter->second.primitives[0].type == iter->second.primitives[0].BOX &&
         (iter->second.primitives[0].dimensions[0] < MAX_GRIPPER_OPEN ||
          iter->second.primitives[0].dimensions[1] < MAX_GRIPPER_OPEN ||
          iter->second.primitives[0].dimensions[2] < MAX_GRIPPER_OPEN)) ||
        (iter->second.primitives[0].type == iter->second.primitives[0].SPHERE &&
         2.0 * iter->second.primitives[0].dimensions[0] <
             MAX_GRIPPER_OPEN)) { // Cylinder or box, is it graspable at all?
      if (iter->second.primitives[0].dimensions[0] > 0.4 ||
          iter->second.primitives[0].dimensions[1] > 0.4 ||
          (iter->second.primitives[0].type == iter->second.primitives[0].BOX &&
           iter->second.primitives[0].dimensions[2] >
               0.4)) { // Too big, probably not an object
        continue;
      }
      float score = iter->second.primitive_poses[0].position.z -
                    (iter->second.primitives[0].dimensions[0] +
                     (iter->second.primitives[0].type == iter->second.primitives[0].CYLINDER
                          ? 4 * iter->second.primitives[0].dimensions[1]
                          : iter->second.primitives[0].dimensions[1] +
                                iter->second.primitives[0].dimensions[2])) /
                        3.0 +
                    (getCategoryOfObject(getNameFromId(iter->second.id)) != 255
                         ? 10.0
                         : 0.0); // Higher is better, smaller is better, we want
                                 // to know what it is TODO add third dimension
      result.insert(
          std::pair<float, moveit_msgs::CollisionObject>(score, iter->second));
    }
  }
  return result;
}

void buildPlanningSceneCB(
    const storing_groceries_msgs::GroceriesGoalConstPtr &/*goal*/) {
  ROS_INFO("buildPlanningSceneCB called");
  storing_groceries_msgs::GroceriesResult result;
  storing_groceries_msgs::GroceriesFeedback feedback;

  group->detachObject(); // Should automatically identify the object to detach,
                         // if there is one
  planning_scene_interface->removeCollisionObjects(
      planning_scene_interface->getKnownObjectNames()); // Clear planning scene
  std::string orig_frame;
  pcl::PointCloud<point_t>::Ptr raw_point_cloud =
      getPointCloud(*nh, tfBuffer, pointcloud_topic, true, &orig_frame);
  ROS_INFO("Getting camera image");
  // Adapted from
  // https://answers.ros.org/question/293890/how-to-use-waitformessage-properly/
  sensor_msgs::Image cam_image;
  boost::shared_ptr<sensor_msgs::Image const> sharedImage =
      ros::topic::waitForMessage<sensor_msgs::Image>(rgb_image_topic, *nh,
                                                     ros::Duration(10, 0));
  if (sharedImage != NULL) {
    cam_image = *sharedImage;
  } else {
    ROS_WARN("Failed to get camera image");
  }

  image_geometry::PinholeCameraModel cam_model;
  boost::shared_ptr<sensor_msgs::CameraInfo const> sharedCameraInfo =
      ros::topic::waitForMessage<sensor_msgs::CameraInfo>(
          rgb_camera_info_topic, *nh, ros::Duration(10, 0));
  if (sharedCameraInfo != NULL) {
    cam_model.fromCameraInfo(*sharedCameraInfo);
  } else {
    ROS_WARN("Failed to get camera info");
  }
  pcl::PointCloud<point_t>::Ptr point_cloud =
      getCloudROI(raw_point_cloud, 0.0, 1.5, -0.7, 0.7, 0.1, 1.5);
  plane_t * table = searchAndAddTableToScene(point_cloud, orig_frame); // Here point_cloud must be in BASE_FRAME
  float table_height=(table!=NULL?table->bbox.center.position.z:0.0);
  printf("Writing: size %lu, width %u, height %u \n", point_cloud->size(),
         point_cloud->width, point_cloud->height);
  try {
    pcl::io::savePCDFile ("point_cloud.pcd", *point_cloud, true);
  } catch (pcl::IOException &e) {
    ROS_WARN("%s", e.what());
  }
  supportSurfaceId = "table";
  pcl::PointCloud<point_t>::Ptr object_cloud(
      new pcl::PointCloud<point_t>());
  ROS_INFO("Making point cloud transform");
  Eigen::Isometry3d transform=getIsometryTransform(tfBuffer, orig_frame, BASE_FRAME);
  pcl::transformPointCloud(*point_cloud, *object_cloud, isometry2affine(transform));
  getObjectsAndAddToPlanningScene(cam_image, cam_model, object_cloud, orig_frame, &transform, table_height); // Here point_cloud must be in the original camera frame
  addArtWalls();

  std::multimap<float, moveit_msgs::CollisionObject> objects =
      chooseObjectsFromPlanningScene();
  printf("Got %lu graspable objects\n", objects.size());
  if (objects.size() == 0) {
    ROS_ERROR("no graspable objects found");
    result.return_code.code = result.return_code.NO_OBJECT_FOUND;
    if ((random() & 1) == 0) {
      result.x = -0.3;
    } else {
      result.yaw = 0.5;
    }
    scene_as->setAborted(result);
    return;
  }

  result.objectIds.resize(objects.size());
  int i = 0;
  for (std::multimap<
           float, moveit_msgs::CollisionObject>::const_reverse_iterator iter =
           objects.crbegin();
       iter != objects.crend(); ++iter) {
    result.objectIds[i++] = iter->second.id;
  }

  result.return_code.code = result.return_code.SUCCESS;
  scene_as->setSucceeded(result);
  ROS_INFO("Planning scene successfully built");
}

// TODO unsuccessful returns, publish feedback, check for preempted
// prerequisites/preconditions:  TODO
// postconditions: object in gripper, lift down, arm close to torso TODO
void pickObjectFromTableCB(
    const storing_groceries_msgs::GroceriesGoalConstPtr & /*goal*/) {
  ROS_INFO("pickObjectFromTableCB called");
  storing_groceries_msgs::GroceriesResult result;
  storing_groceries_msgs::GroceriesFeedback feedback;
  moveit::planning_interface::MoveItErrorCode moveit_result;
  isObjectInGripper = false;
  // Go to a perception position: lift up?, arm/gripper not in field of view
  // group->setJointValueTarget("torso_lift_joint", 0.34);
  // group->move();
  /*std::vector<double> joint_values = { 0.34, CARRY_POSE };//arm to the right,
  elbow pointing down, eef pointing up
  group->setJointValueTarget(joint_values); //In order: torso_lift_joint
  arm_1_joint arm_2_joint arm_3_joint arm_4_joint arm_5_joint arm_6_joint
  arm_7_joint
  moveit_result=group->move();//this can be dangerous because the planning scene
  is not there yet
  if(!moveit_result) {
      result.return_code.code=result.return_code.MOVE_FAILED;
      pick_as->setAborted(result);
      return;
  }*/

  // planning_scene_interface->removeCollisionObjects(planning_scene_interface->getKnownObjectNames());//Clear
  // planning scene
  // std::string supportSurfaceId=getObjectsAndAddToPlanningScene();
  // addRandomCollisionObject();
  supportSurfaceId = "table";
  std::multimap<float, moveit_msgs::CollisionObject> objects =
      chooseObjectsFromPlanningScene();

  // Erase what you don't want to pick
  /*for (auto = objects.begin(); iter != objects.end();) {
    bool keep=false;
    for (auto object : goal.objects) {
      if(getNameFromId(iter->second.id) != "iso_drink") {
        keep=;
      }
    }
    if (!keep) {
      iter = objects.erase(iter);
      ROS_INFO("Erasing object %s", iter->second.id);
    } else {
      ++iter;
    }
  }*/

  printf("Got %lu graspable objects\n", objects.size());
  if (objects.size() == 0) {
    ROS_ERROR("no graspable objects found");
    result.return_code.code = result.return_code.NO_OBJECT_FOUND;
    pick_as->setAborted(result);
    return;
  }

  feedback.status.code = feedback.status.PERCEPTION_DONE;
  pick_as->publishFeedback(feedback);

  if (pick_as->isPreemptRequested() || !ros::ok()) {
    ROS_INFO("Preempted");
    pick_as->setPreempted(); // set the action state to preempted
    return;
  }

  ROS_INFO("pick");
  // Go to near-grasping-position? From experience: not so helpful
  /*//std::vector<double> joint_values2 = { 0.34, 0.64, 0.0, -1.68, 2.29, 1.62,
  1.55, 1.74 };//elbow to the right
  std::vector<double> joint_values2 = { 0.34, 1.56, -1.5, -3.12, 2.28, 1.41,
  -0.65, 0.0 };//elbow down
  group->setJointValueTarget(joint_values2); //In order: torso_lift_joint
  arm_1_joint arm_2_joint arm_3_joint arm_4_joint arm_5_joint arm_6_joint
  arm_7_joint
  group->setMaxVelocityScalingFactor(0.5);
  moveit_result=group->move();
  if(!moveit_result) {
      result.return_code.code=result.return_code.MOVE_FAILED;
      pick_as->setAborted(result);
      return;
  }*/

  // Loop through all graspable objects, beginning with the most promising. If a
  // pick succeeds, exit the loop
  for (std::multimap<
           float, moveit_msgs::CollisionObject>::const_reverse_iterator iter =
           objects.crbegin();
       iter != objects.crend(); ++iter) {
    std::vector<moveit_msgs::Grasp> grasps = generateGrasps(iter->second);
    if (grasps.size() == 0) {
      ROS_WARN("No grasps generated");
      // TODO continue or not? moveit might try a default grasp and succeed?
    }
    printf("executing grasp now, object=%s, score=%f, supportSurface=%s\n",
           iter->second.id.c_str(), iter->first, supportSurfaceId.c_str());
    group->setSupportSurfaceName(supportSurfaceId);
    moveit_result =
        my_pick(group, iter->second.id, grasps, supportSurfaceId,
                false); // true for only planning, false for also moving
    if (moveit_result) {
      ROS_INFO("pick successful");
      isObjectInGripper = true;
      objectInGripper = iter->second; // TODO
      // feedback.code=feedback.;
      // pick_as->publishFeedback(feedback);
      break;
    } else {
      printf("pick failed with code %i, trying next object (if available)\n",
             moveit_result.val);
    }
  }
  group->setSupportSurfaceName(""); // We do not want the grasped object to
                                    // collide with the table (in the next
                                    // movements)

  // Go to a transport position: lift down, arm close to torso,
  // group->setNamedTarget("transport2");
  std::vector<double> joint_values4 = {
      0.05,
      CARRY_POSE}; // arm to the right, elbow pointing down, eef pointing up
  group->setJointValueTarget(joint_values4); // In order: torso_lift_joint
                                             // arm_1_joint arm_2_joint
                                             // arm_3_joint arm_4_joint
                                             // arm_5_joint arm_6_joint
                                             // arm_7_joint
  moveit_result = group->move();
  if (!moveit_result) {
    result.return_code.code = result.return_code.MOVE_FAILED;
    pick_as->setAborted(result);
    ROS_ERROR("Aborting, move failed");
    return;
  }

  // TODO check again if object is in gripper
  ROS_INFO("Gripper opening: %f\n",
           getGripperOpening()); // TODO does not work for objects like cloth
  if (!isObjectInGripper ||
      planning_scene_interface->getAttachedObjects().size() == 0 ||
      getGripperOpening() < 0.01) {
    ROS_ERROR("No object in gripper, pick failed");
    if (fabs(atan2(objects.crbegin()->second.primitive_poses[0].position.y,
                   objects.crbegin()->second.primitive_poses[0].position.x)) >
        0.35) {
      result.yaw =
          -atan2(objects.crbegin()->second.primitive_poses[0].position.y,
                 objects.crbegin()->second.primitive_poses[0].position.x);
    } else if (fabs(objects.crbegin()->second.primitive_poses[0].position.x -
                    0.8) > 0.2) {
      result.x = objects.crbegin()->second.primitive_poses[0].position.x - 0.8;
    }
    result.return_code.code = result.return_code.PICK_FAILED;
    pick_as->setAborted(result);
    ROS_ERROR("Aborting, pick failed");
    return;
  }

  result.return_code.code = result.return_code.SUCCESS;
  pick_as->setSucceeded(result);
  ROS_INFO("Pick finished successfully");
}

// TODO unsuccessful returns
// prerequisites/preconditions: Object in gripper, lift down, arm close to torso
// TODO
// postconditions: No object in gripper, lift down, arm close to torso TODO
void placeObjectOnShelfCB(
    const storing_groceries_msgs::GroceriesGoalConstPtr & /*goal*/) {
  ROS_INFO("placeObjectOnShelfCB called");
  storing_groceries_msgs::GroceriesResult result;
  storing_groceries_msgs::GroceriesFeedback feedback;
  moveit::planning_interface::MoveItErrorCode moveit_result;
  planning_scene_interface->removeCollisionObjects(
      planning_scene_interface->getKnownObjectNames()); // Clear planning scene,
                                                        // but the attached
                                                        // object(s) obviously
                                                        // have to be kept
  std::map<std::string, moveit_msgs::AttachedCollisionObject> attachedObjects =
      planning_scene_interface->getAttachedObjects();
  ROS_INFO_STREAM("Number of attached objects: " << attachedObjects.size());
  if (attachedObjects.size() == 1)
    objectInGripper = attachedObjects.cbegin()->second.object;
  ROS_INFO_STREAM("objectInGripper=" << objectInGripper);
  // TODO check isObjectInGripper

  std::string orig_frame;
  float shelfx = 0.0, shelfy = 0.0, shelfangle = 0.0;
  pcl::PointCloud<point_t>::Ptr point_cloud;

  moveHead(-0.7);

  point_cloud = getCloudROI(
      getPointCloud(*nh, tfBuffer, pointcloud_topic, true, &orig_frame), 0.0,
      2.0, -0.7, 0.7, 0.1, 2.0);
  ROS_INFO("Getting camera image");
  // Adapted from
  // https://answers.ros.org/question/293890/how-to-use-waitformessage-properly/
  sensor_msgs::Image cam_image;
  boost::shared_ptr<sensor_msgs::Image const> sharedImage =
      ros::topic::waitForMessage<sensor_msgs::Image>(rgb_image_topic, *nh,
                                                     ros::Duration(10, 0));
  if (sharedImage != NULL) {
    cam_image = *sharedImage;
  } else {
    ROS_WARN("Failed to get camera image");
  }

  image_geometry::PinholeCameraModel cam_model;
  boost::shared_ptr<sensor_msgs::CameraInfo const> sharedCameraInfo =
      ros::topic::waitForMessage<sensor_msgs::CameraInfo>(
          rgb_camera_info_topic, *nh, ros::Duration(10, 0));
  if (sharedCameraInfo != NULL) {
    cam_model.fromCameraInfo(*sharedCameraInfo);
  } else {
    ROS_WARN("Failed to get camera info");
  }
  if (searchForShelf(point_cloud, shelfx, shelfy,
                     shelfangle)) { // TODO if getCloudROI filters everthing,
                                     // there occurs an error somewhere here
    ROS_INFO("Found shelf: x=%f, y=%f, angle=%f\n", shelfx, shelfy, shelfangle);
  } else {
    ROS_WARN("No shelf found");
    // TODO just add the oobbs?
  }
  printf("Writing: size %lu, width %u, height %u \n", point_cloud->size(),
         point_cloud->width, point_cloud->height);
  try {
    pcl::io::savePCDFile ("point_cloud_shelf.pcd", *point_cloud, true);
  } catch (pcl::IOException &e) {
    ROS_WARN("%s", e.what());
  }
  pcl::PointCloud<point_t>::Ptr object_cloud1(
      new pcl::PointCloud<point_t>());
  Eigen::Isometry3d transform=getIsometryTransform(tfBuffer, orig_frame, BASE_FRAME);
  pcl::transformPointCloud(*point_cloud, *object_cloud1, isometry2affine(transform));
  getObjectsAndAddToPlanningScene(cam_image, cam_model, object_cloud1, orig_frame, &transform, 0.0); // Here point_cloud must be in the original camera frame TODO check result

  ROS_INFO("Moving up ...");
  // group->setJointValueTarget("torso_lift_joint", 0.34); //Seems to not be a good idea, because the arm will move straight to the front (will not maintain the position)
  // std::vector<double> joint_values = { 0.34, CARRY_POSE };//arm to the right,
  // elbow pointing down, eef pointing up
  // group->setJointValueTarget(joint_values); //In order: torso_lift_joint arm_1_joint
  // group->move();
  // TODO set gaze
  moveHead(0.0);

  point_cloud = getCloudROI(
      getPointCloud(*nh, tfBuffer, pointcloud_topic, true, &orig_frame), 0.0,
      2.0, -0.7, 0.7, 0.1, 2.0);
  ROS_INFO("Getting camera image");
  // Adapted from
  // https://answers.ros.org/question/293890/how-to-use-waitformessage-properly/
  sharedImage = ros::topic::waitForMessage<sensor_msgs::Image>(rgb_image_topic, *nh, ros::Duration(10, 0));
  if (sharedImage != NULL) {
    cam_image = *sharedImage;
  } else {
    ROS_WARN("Failed to get camera image");
  }

  sharedCameraInfo = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(
          rgb_camera_info_topic, *nh, ros::Duration(10, 0));
  if (sharedCameraInfo != NULL) {
    cam_model.fromCameraInfo(*sharedCameraInfo);
  } else {
    ROS_WARN("Failed to get camera info");
  }
  if (searchForShelf(point_cloud, shelfx, shelfy, shelfangle)) {
    if (shelfangle < -M_PI / 2.0 || shelfangle > M_PI / 2.0) {
      shelfangle = angleModulo(shelfangle + M_PI);
    }
    ROS_INFO("Found shelf: x=%f, y=%f, angle=%f\n", shelfx, shelfy, shelfangle);
    addShelfToPlanningScene(shelfx, shelfy, shelfangle);
  } else {
    ROS_WARN("No shelf found");
    if (shelfx == 0.0)
      shelfx = 0.7;
    shelfy = 0.0;
    shelfangle = 0.0;
    addShelfToPlanningScene(shelfx, shelfy, shelfangle);
    // TODO just add the oobbs?
  }
  pcl::PointCloud<point_t>::Ptr object_cloud2( new pcl::PointCloud<point_t>());
  Eigen::Isometry3d transform2=getIsometryTransform(tfBuffer, orig_frame, BASE_FRAME);
  pcl::transformPointCloud(*point_cloud, *object_cloud2, isometry2affine(transform2));
  getObjectsAndAddToPlanningScene(cam_image, cam_model, object_cloud2, orig_frame, &transform2, 0.0); // Here point_cloud must be in the
                                       // original camera frame TODO check
                                       // result //TODO ids might be the same
                                       // and the object overwritten!
  addArtWalls();
  // TODO remove duplicates from planning scene

  // std::string
  // supportSurfaceId="shelf_floor2"/*getObjectsAndAddToPlanningScene()*/;//TODO
  // std::string objectInGripper; //getAttachedObjects()?
  double x, y, z, sizex, sizey, angle;
  supportSurfaceId =
      computeTargetPosition(objectInGripper.id, x, y, z, sizex, sizey,
                            angle, shelfx, shelfy, shelfangle);
  ROS_INFO_STREAM("computeTargetPosition returned supportSurfaceId="
                  << supportSurfaceId << ", x=" << x << ", y=" << y
                  << ", z=" << z << ", sizex=" << sizex << ", sizey=" << sizey
                  << ", angle=" << angle);
  x = shelfx; // 0.8;
  y = shelfy; // 0.0;
  // z=0.95;//TODO this is the middle between two shelf floors minus 0.05
  // sizex=0.4;
  // sizey=0.4;
  // angle=shelfangle;//0.0;
  std::vector<moveit_msgs::PlaceLocation> places =
      generatePlaces(objectInGripper, x, y, z, sizex, sizey, angle);
  if (places.size() == 0) {
    // TODO
    result.return_code.code = result.return_code.NO_PLACE_LOC_FOUND;
    place_as->setAborted();
    return; // TODO we could try to just keep going, moveit might place it
            // somewhere?
  }
  group->setSupportSurfaceName(supportSurfaceId); // TODO
  feedback.status.code = feedback.status.PERCEPTION_DONE;
  place_as->publishFeedback(feedback);
  ROS_INFO("Placing in 10 sec ...");
  ros::WallDuration(10.0).sleep();
  if (place_as->isPreemptRequested() || !ros::ok()) {
    ROS_INFO("Preempted");
    place_as->setPreempted(); // set the action state to preempted
    return;
  }
  // moveit_result=group->place(objectInGripper.id, places, false);//true=plan
  // only, false=also execute
  moveit_result =
      my_place(group, objectInGripper.id, places, supportSurfaceId, false);
  if (!moveit_result) {
    printf("place failed with code %i\n", moveit_result.val);
    // TODO
    // TODO try other heights?
  } else {
    ROS_INFO("place succeeded");
    isObjectInGripper =
        false; // to indicate that no object is in the gripper any more
  }
  group->setSupportSurfaceName("");

  ROS_INFO("Moving back to start position");
  std::vector<double> joint_values3 = {
      0.05,
      CARRY_POSE}; // arm to the right, elbow pointing down, eef pointing up
  group->setJointValueTarget(joint_values3); // In order: torso_lift_joint
                                             // arm_1_joint arm_2_joint
                                             // arm_3_joint arm_4_joint
                                             // arm_5_joint arm_6_joint
                                             // arm_7_joint
  moveit_result = group->move();
  if (!moveit_result) {
    result.return_code.code = result.return_code.MOVE_FAILED;
    place_as->setAborted(result);
    return;
  }

  if (isObjectInGripper) {
    result.return_code.code = result.return_code.PLACE_FAILED;
    place_as->setAborted(result);
    return;
  }

  result.return_code.code = result.return_code.SUCCESS;
  place_as->setSucceeded(result);
  ROS_INFO("Place finished successfully");
}

// clustered must be in BASE_FRAME
void searchForDishwasher(pcl::PointCloud<point_t>::Ptr clustered,
                         float *x, float *y, float *angle) {
  // ROS_INFO("searchForDishwasher called, cloud has size %lu", point_cloud->size());
  *x = 0;
  *y = 0;
  *angle = 0;
  // if(point_cloud->size()==0) {
  //    ROS_WARN("searchForDishwasher(): cloud is empty, so returning");
  //}

  deleteAllPlaneMarkers();
  // pcl::PointCloud<point_t>::Ptr clustered=cluster(point_cloud);//TODO
  // cluster might "destroy" the point_cloud?
  ROS_INFO_STREAM("Clustered point cloud, size: " << clustered->size());
  // pcl::PointCloud<point_t>::Ptr cloud_pointer(&clustered);
  std::vector<plane_t> planes = findPlanes(clustered, 2);
  ROS_INFO("searchForDishwasher: got %lu planes", planes.size());
  for (size_t i = 0; i < planes.size(); i++) {
    std::cout << "plane: " << planes[i].bbox << std::endl;
    tf2::Quaternion q1;
    tf2::fromMsg(planes[i].bbox.center.orientation, q1);
    tf2::Transform trans1(q1);
    tf2::Vector3 vec1(1.0, 0.0, 0.0);
    vec1 = trans1 * vec1;
    for (size_t j = i + 1; j < planes.size(); j++) {
      tf2::Quaternion q2;
      tf2::fromMsg(planes[j].bbox.center.orientation, q2);
      tf2::Transform trans2(q2);
      tf2::Vector3 vec2(1.0, 0.0, 0.0);
      vec2 = trans2 * vec2;
      std::cout << "angle: "
                << fabs(acos(vec1.x() * vec2.x() + vec1.y() * vec2.y() +
                             vec1.z() * vec2.z()))
                << std::endl;
      // std::cout << q1 << " " << q2 << std::endl;
      std::cout << "vec1: " << vec1.x() << " " << vec1.y() << " " << vec1.z()
                << std::endl;
      std::cout << "vec2: " << vec2.x() << " " << vec2.y() << " " << vec2.z()
                << std::endl;
      if (fabs(acos(vec1.x() * vec2.x() + vec1.y() * vec2.y() +
                    vec1.z() * vec2.z()) -
               M_PI / 2.0) < 0.05) {
        std::cout << "Perpendicular planes:" << i << " " << j << std::endl;
        // Make sure vec1 and vec2 point "inward", else flip them
        if (signedDistancePointToPlane(
                vec1.x(), vec1.y(), vec1.z(),
                vec1.x() * planes[i].bbox.center.position.x +
                    vec1.y() * planes[i].bbox.center.position.y +
                    vec1.z() * planes[i].bbox.center.position.z,
                planes[j].bbox.center.position.x,
                planes[j].bbox.center.position.y,
                planes[j].bbox.center.position.z) < 0)
          vec1 = -vec1;
        if (signedDistancePointToPlane(
                vec2.x(), vec2.y(), vec2.z(),
                vec2.x() * planes[j].bbox.center.position.x +
                    vec2.y() * planes[j].bbox.center.position.y +
                    vec2.z() * planes[j].bbox.center.position.z,
                planes[i].bbox.center.position.x,
                planes[i].bbox.center.position.y,
                planes[i].bbox.center.position.z) < 0)
          vec2 = -vec2;
        vision_msgs::BoundingBox3D horizontal;
        vision_msgs::BoundingBox3D vertical;

        if ((pow(vec1.x(), 2.0) + pow(vec1.y(), 2.0) +
             pow(vec1.z() + 1.0, 2.0)) <
            (pow(vec2.x(), 2.0) + pow(vec2.y(), 2.0) +
             pow(vec2.z() + 1.0, 2.0))) {
          horizontal = planes[i].bbox;
          vertical = planes[j].bbox;
          *x = planes[i].bbox.center.position.x;
          *y = planes[i].bbox.center.position.y;
          *angle = atan2(vec2.y(), vec2.x());
        } else {
          horizontal = planes[j].bbox;
          vertical = planes[i].bbox;
          *x = planes[j].bbox.center.position.x;
          *y = planes[j].bbox.center.position.y;
          *angle = atan2(vec1.y(), vec1.x());
        }
      }
    }
    std::cout << "yaw="
              << 2.0 * acos(planes[i].bbox.center.orientation.w /
                            sqrt(planes[i].bbox.center.orientation.w *
                                     planes[i].bbox.center.orientation.w +
                                 planes[i].bbox.center.orientation.y *
                                     planes[i].bbox.center.orientation.y))
              << std::endl;
    //*angle+=2.0*acos(planes[i].bbox.center.orientation.w/sqrt(planes[i].bbox.center.orientation.w*planes[i].bbox.center.orientation.w+planes[i].bbox.center.orientation.y*planes[i].bbox.center.orientation.y));
  }
}

void addWasherToScene(float x, float y, float angle) {
  ROS_INFO("addWasherToScene called");
  float washer_depth = 0.6, washer_width = 0.6, washer_height = 0.845,
        safety = 0.04;
  tf2::Quaternion orientation;
  orientation.setRPY(0.0, 0.0, angle);
  std::vector<moveit_msgs::CollisionObject> washer_parts;
  moveit_msgs::CollisionObject washer;
  washer.header.frame_id = BASE_FRAME;
  washer.id = "washer";
  washer.primitives.resize(2);
  washer.primitive_poses.resize(2);

  // main body
  washer.primitives[0].type = washer.primitives[0].BOX;
  washer.primitives[0].dimensions.resize(3);
  washer.primitives[0].dimensions[0] = washer_width + safety;
  washer.primitives[0].dimensions[1] = washer_depth + safety;
  washer.primitives[0].dimensions[2] = washer_height + safety;
  washer.primitive_poses[0].position.x =
      x; //+sin(angle)*washer_width/2.0-cos(angle)*washer_depth/2.0;
  washer.primitive_poses[0].position.y =
      y; //-cos(angle)*washer_width/2.0+sin(angle)*washer_depth/2.0;
  washer.primitive_poses[0].position.z = washer_height / 2.0;
  washer.primitive_poses[0].orientation = tf2::toMsg(orientation);

  washer.primitives[1].type = washer.primitives[1].BOX;
  washer.primitives[1].dimensions.resize(3);
  washer.primitives[1].dimensions[0] = washer_width + safety;
  washer.primitives[1].dimensions[1] = 0.65 + safety;
  washer.primitives[1].dimensions[2] = 0.7 + safety;
  washer.primitive_poses[1].position.x =
      x + sin(angle) * (washer_depth + 0.65) / 2.0;
  washer.primitive_poses[1].position.y =
      y - cos(angle) * (washer_depth + 0.65) / 2.0;
  washer.primitive_poses[1].position.z = (0.7 + safety) / 2.0;
  washer.primitive_poses[1].orientation = tf2::toMsg(orientation);

  washer_parts.push_back(washer);

  if (planning_scene_interface != NULL) {
    planning_scene_interface->applyCollisionObjects(washer_parts);
  } else {
    ROS_WARN("addShelfToPlanningScene(): planning_scene_interface is NULL");
  }
}

/*void buildCleanTableSceneCB(const
   storing_groceries_msgs::GroceriesGoalConstPtr &goal) {
    ROS_INFO("buildCleanTableSceneCB called");
    storing_groceries_msgs::GroceriesResult result;
    storing_groceries_msgs::GroceriesFeedback feedback;
    planning_scene_interface->removeCollisionObjects(planning_scene_interface->getKnownObjectNames());//Clear
   planning scene

    moveHead(-0.5, 0.5);
    std::string orig_frame;
    pcl::PointCloud<point_t>::Ptr pointCloud=getCloudROI(getPointCloud(*nh,
   tfBuffer, pointcloud_topic, true, &orig_frame), 0.0, 2.0, -1.0, 1.0, 0.1,
   2.0);
    moveHead(-0.5, 0.0);
    float x, y, angle;
    searchForDishwasher(pointCloud, &x, &y, &angle);*/
/*std::vector<vision_msgs::BoundingBox3D> planes=findPlanes(getPointCloud(*nh,
tfBuffer, pointcloud_topic), 2);
for(size_t i=0; i<planes.size(); i++) {
    moveit_msgs::CollisionObject collision_object;
    collision_object.id = i+64;
    collision_object.header.frame_id=BASE_FRAME;
    collision_object.primitives.resize(1);
    collision_object.primitives[0].type = collision_object.primitives[0].BOX;
    collision_object.primitives[0].dimensions.resize(3);
    collision_object.primitives[0].dimensions[0] =
(planes[i].size.x<0.02?0.05:planes[i].size.x+0.05);
    collision_object.primitives[0].dimensions[1] =
(planes[i].size.y<0.02?0.05:planes[i].size.y+0.05);
    collision_object.primitives[0].dimensions[2] =
(planes[i].size.z<0.02?0.05:planes[i].size.z+0.05);
    collision_object.primitive_poses.resize(1);
    collision_object.primitive_poses[0] = planes[i].center;
    collision_object.operation=collision_object.ADD;
    if(planning_scene_interface!=NULL) {
        planning_scene_interface->applyCollisionObject(collision_object);
    }
}*/

// getObjectsAndAddToPlanningScene(0.0);//TODO put this back in
/*addArtWalls();

result.return_code.code=result.return_code.SUCCESS;
clean_as->setSucceeded(result);
ROS_INFO("Planning scene successfully built");
}*/

int main(int argc, char **argv) {
  // return 0;
  ros::init(argc, argv, "storing_groceries_server");
  ros::NodeHandle new_handle("~");
  nh = &new_handle;
  nh->param<std::string>("pointcloud_topic", pointcloud_topic,
                         "/xtion/depth_registered/points");
  nh->param<std::string>("rgb_image_topic", rgb_image_topic,
                         "/xtion/rgb/image_raw");
  std::string objects_yaml_path;
  nh->getParam("objects_config_path", objects_yaml_path);
  std::cout << "Got objects_config_path:>" << objects_yaml_path << "<"
            << std::endl;
  parseConfigYAML(objects_yaml_path);
  /*for(size_t i=0; i<categories.size(); i++) {
      puts(categories[i].c_str());
  }
  for(size_t i=0; i<possibleObjects.size(); i++) {
      std::cout << possibleObjects[i] << std::endl;
  }*/
  ROS_INFO("Creating service clients and action servers, advertising topics ...");
  init_high_level(nh, &tfBuffer);
  init_grasp_publisher(*nh);
  init_plane_publisher(*nh);
  tf2_ros::TransformListener newListener(tfBuffer);
  tfListener = &newListener;
  // ros::AsyncSpinner spinner(1);
  // spinner.start();

  // For testing
  // searchAndAddTableToScene();
  // pcl::PointCloud<point_t>::Ptr cloud=getPointCloud(*nh, tfBuffer,
  // pointcloud_topic, false);
  // findPlanes(cloud, 2);
  // try {
  //   pcl::io::savePCDFile ("point_cloud.pcd", *cloud, true);
  // } catch (pcl::IOException &e) {
  //   ROS_WARN("%s", e.what());
  // }
  // getObjectsAndAddToPlanningScene(0.0);

  /*std::string orig_frame;
  pcl::PointCloud<point_t>::Ptr raw_point_cloud=getPointCloud(*nh,
  tfBuffer, pointcloud_topic, true, &orig_frame);
  //point_t rawMinPoint, rawMaxPoint, minPoint, maxPoint;
  //pcl::getMinMax3D(*raw_point_cloud, rawMinPoint, rawMaxPoint);
  pcl::PointCloud<point_t>::Ptr point_cloud=getCloudROI(raw_point_cloud,
  0.0, 2.0, -0.7, 0.7, 0.1, 2.0);
  //pcl::getMinMax3D(*point_cloud, minPoint, maxPoint);
  float table_height=searchAndAddTableToScene(point_cloud);//Here point_cloud
  must be in BASE_FRAME
  printf("Writing: size %lu, width %u, height %u \n", point_cloud->size(),
  point_cloud->width, point_cloud->height);
  try {
      pcl::io::savePCDFile ("point_cloud.pcd", *point_cloud, true);
  } catch(pcl::IOException &e) {
      ROS_WARN("%s", e.what());
  }
  supportSurfaceId="table";
  pcl::PointCloud<point_t>::Ptr object_cloud(new
  pcl::PointCloud<point_t> ());
  ROS_INFO("Making point cloud transform");
  pcl::transformPointCloud (*point_cloud, *object_cloud,
  getAffineTransform(tfBuffer, orig_frame, BASE_FRAME));
  getObjectsAndAddToPlanningScene(table_height, object_cloud, orig_frame);//Here
  point_cloud must be in the original camera frame
  */
  /*std::string orig_frame;
    pcl::PointCloud<point_t>::Ptr point_cloud=getPointCloud(*nh, tfBuffer, pointcloud_topic, false, &orig_frame);
    point_t rawMinPoint, rawMaxPoint, minPoint, maxPoint;
    //pcl::getMinMax3D(*raw_point_cloud, rawMinPoint, rawMaxPoint);
    //pcl::PointCloud<point_t>::Ptr point_cloud=getCloudROI(raw_point_cloud, 0.0, 2.0, -0.7, 0.7, 0.1, 2.0);
    //pcl::getMinMax3D(*point_cloud, minPoint, maxPoint);
    //if(minPoint.x-rawMinPoint.x<0.05) { //Not such a good test
    //    ROS_WARN_STREAM("Near table edge probably not detected: minPoint.x=" << minPoint.x << " rawMinPoint.x=" << rawMinPoint.x);
    //}
    float table_height=searchAndAddTableToScene(point_cloud, orig_frame);//Here point_cloud must be in BASE_FRAME
    printf("Writing: size %lu, width %u, height %u \n", point_cloud->size(), point_cloud->width, point_cloud->height);
    try {
        pcl::io::savePCDFile ("point_cloud.pcd", *point_cloud, true);
    } catch(pcl::IOException &e) {
        ROS_WARN("%s", e.what());
    }
    //supportSurfaceId="table";
    //pcl::PointCloud<point_t>::Ptr object_cloud(new pcl::PointCloud<point_t> ());
    //ROS_INFO("Making point cloud transform");
    //pcl::transformPointCloud (*point_cloud, *object_cloud, getAffineTransform(tfBuffer, orig_frame, BASE_FRAME));
    getObjectsAndAddToPlanningScene(0.0, point_cloud, orig_frame);//Here point_cloud must be in the original camera frame
    addArtWalls();
    */ // Testing end

  ros::WallDuration(1.0).sleep();
  ROS_INFO("Creating planning scene interface and move group interface ...");
  moveit::planning_interface::PlanningSceneInterface new_planning_scene_interface;
  planning_scene_interface = &new_planning_scene_interface;

  // For testing
  /*planning_scene_interface->removeCollisionObjects(planning_scene_interface->getKnownObjectNames());//Clear
  planning scene

  moveHead(-0.5, -0.5);
  std::string orig_frame;
  pcl::PointCloud<point_t>::Ptr pointCloud=getCloudROI(getPointCloud(*nh,
  tfBuffer, pointcloud_topic, true, &orig_frame), 0.0, 2.0, -1.0, 1.0, 0.1,
  2.0);
  moveHead(-0.5, 0.0);
  float x, y, angle;
  searchForDishwasher(pointCloud, &x, &y, &angle);*/
  // planning_scene_interface->removeCollisionObjects(planning_scene_interface->getKnownObjectNames());//Clear
  // planning scene
  // std::string supportSurfaceId=getObjectsAndAddToPlanningScene(0.0);
  // float shelfx, shelfy, shelfangle;
  // searchForShelf(&shelfx, &shelfy, &shelfangle);
  // ROS_INFO("Found shelf: x=%f, y=%f, angle=%f\n", shelfx, shelfy,
  // shelfangle);
  // addShelfToPlanningScene(shelfx, shelfy, shelfangle);

  moveit::planning_interface::MoveGroupInterface new_group("arm_torso"); // can be arm or arm_torso
  group = &new_group;

  ROS_INFO("Creating place action client");
  initActionClients(*nh);

  ROS_INFO("Setting move group parameters");
  // group->setPlannerId("PRM"); //PRM or RRT
  group->setPlanningTime(10.0); // Default is 5.0s?
  // TODO can look around, can replan, ...
  ros::WallDuration(1.0).sleep();

  std::cout << "PlannerId:" << group->getPlannerId() << std::endl;
  std::map<std::string, std::string> params =
      group->getPlannerParams(group->getPlannerId(), group->getName());
  std::cout << "Planner params: " << params.size() << std::endl;
  for (std::map<std::string, std::string>::const_iterator iter = params.begin();
       iter != params.end(); ++iter) {
    std::cout << "Key: " << iter->first << std::endl
              << "Values:" << iter->second << std::endl;
  }
  std::cout << "Planning time=" << group->getPlanningTime() << std::endl;

  // Create ActionServers last because we only want to accept callbacks when
  // everything else is set up
  actionlib::SimpleActionServer<storing_groceries_msgs::GroceriesAction>
      new_scene_as(*nh, "buildScene", boost::bind(&buildPlanningSceneCB, _1),
                   false);
  scene_as = &new_scene_as;
  scene_as->start();
  // actionlib::SimpleActionServer<storing_groceries_msgs::GroceriesAction>
  // new_clean_as(*nh, "buildCleanTableScene",
  // boost::bind(&buildCleanTableSceneCB, _1), false);
  // clean_as=&new_clean_as;
  // clean_as->start();
  actionlib::SimpleActionServer<storing_groceries_msgs::GroceriesAction>
      new_pick_as(*nh, "pickObject", boost::bind(&pickObjectFromTableCB, _1),
                  false);
  pick_as = &new_pick_as;
  pick_as->start();
  actionlib::SimpleActionServer<storing_groceries_msgs::GroceriesAction>
      new_place_as(*nh, "placeObject", boost::bind(&placeObjectOnShelfCB, _1),
                   false);
  place_as = &new_place_as;
  place_as->start();

  ROS_INFO("Action servers are running, node is reading, spinning ...");

  // Testing
  /*addRandomCollisionObject();
  std::multimap<float,moveit_msgs::CollisionObject>
  objects=chooseObjectsFromPlanningScene();
  printf("size: %lu\n", objects.size());
  for (std::multimap<float,moveit_msgs::CollisionObject>::const_reverse_iterator
  iter = objects.crbegin(); iter != objects.crend(); ++iter) {
      std::vector<moveit_msgs::Grasp> grasps=generateGrasps(iter->second);
      my_pick(group, iter->second.id, grasps, "", false);
  }*/

  ros::spin(); // For the action servers
  return 0;
}
