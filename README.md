# Storing Groceries Object Detection
## util
Contains utility functions, e.g. geometric functions to compute angles and distances.

## object_knowledge
Information about objects, e.g. what category do they belong to, what shape do they have.

## high_level_functions
### getObjectsAndAddToPlanningScene
Input: point cloud and rgb image. Segments the cloud and treats every cluster as a detection/object. Then tries to find out more about the objects (size, position, orientation, shape, category) using the known information about the possible objects as well as e.g. classification on rgb images and shape fitting (sq fitting or sample consensus based fitting). All the information is brought together inter alia by Kalman-like fusing.

## point_clouds
Contains everything related to point clouds, e.g. finding planes, cylinders, spheres, or boxes in clouds via sample consensus; box filters, or computing the minimum bounding box in 2 dimensions.

## pick_place
Everything related to manipulation/pick/place, e.g. computing grasps and place locations, and adapted functions from moveit
